package fr.gendarmerie.stsisi.adr.ui.fragment.information.agreement;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Objects;

import fr.gendarmerie.stsisi.adr.R;
import fr.gendarmerie.stsisi.adr.layout.AgreementDialog;
import fr.gendarmerie.stsisi.adr.utilities.App;

public class AgreementDialogFragment extends DialogFragment {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    private static final String TAG = AgreementDialogFragment.class.getSimpleName();

    private Button dialog_fragment_disclaimer_button_close;
    private Switch dialog_fragment_disclaimer_switch_accept;
    private TextView dialog_fragment_disclaimer_textView_content;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_fragment_agreement, container, false);

        dialog_fragment_disclaimer_button_close = v.findViewById(R.id.dialog_fragment_disclaimer_button_close);
        dialog_fragment_disclaimer_switch_accept = v.findViewById(R.id.dialog_fragment_disclaimer_switch_accept);
        dialog_fragment_disclaimer_textView_content = v.findViewById(R.id.dialog_fragment_disclaimer_textView_content);

        setViews();

        setListeners();

        return v;
    }

    @Override
    public void onStart()
    {
        super.onStart();

        if (getDialog() == null)
            return;

        int dialogW = (int)(App.getWidth()*0.9);
        int dialogH = (int)(App.getHeight()*0.9);

        int orientation = this.getResources().getConfiguration().orientation;
        if(getResources().getBoolean(R.bool.isTablet)){
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                Objects.requireNonNull(getDialog().getWindow()).setLayout(dialogW, dialogH);
            } else {
                Objects.requireNonNull(getDialog().getWindow()).setLayout(dialogH, dialogW);
            }
        }
        else{
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                Objects.requireNonNull(getDialog().getWindow()).setLayout(dialogW, dialogH);
            } else {
                Objects.requireNonNull(getDialog().getWindow()).setLayout(dialogH, dialogW);
            }
        }
    }

    private void setViews(){
        dialog_fragment_disclaimer_textView_content.setText(getDisclaimerFromFile());
        enableButtonClose(false);
    }

    private void setListeners(){
        dialog_fragment_disclaimer_button_close.setOnClickListener(v -> this.dismiss());
        dialog_fragment_disclaimer_switch_accept.setOnCheckedChangeListener((button, checked) -> onCheckedChange(checked));

    }

    private void onCheckedChange(boolean checked) {
        AgreementDialog.accept(checked);
        enableButtonClose(checked);
    }

    private void enableButtonClose(Boolean checked){
        dialog_fragment_disclaimer_button_close.setEnabled(checked);
    }

    private Spanned getDisclaimerFromFile(){

        InputStream inputStream = getResources().openRawResource(R.raw.agreement);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        StringBuilder disclaimer = new StringBuilder();

        try {
            String line;
            while ((line = reader.readLine()) != null) {
                if(!line.equals("")) {
                    disclaimer.append(line).append("<br />");
                }
                else{
                    disclaimer.append("<br />");
                }
            }
            reader.close();
        }
        catch(IOException exception){
            Log.e(TAG, "Error while reading disclaimer.txt text file");
        }

        return Html.fromHtml(disclaimer.toString());
    }
}
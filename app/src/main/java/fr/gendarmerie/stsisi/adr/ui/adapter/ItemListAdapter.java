package fr.gendarmerie.stsisi.adr.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.gendarmerie.stsisi.adr.R;
import fr.gendarmerie.stsisi.adr.db.entities.Symbol;
import fr.gendarmerie.stsisi.adr.interfaces.listener.RecyclerViewClickListener;
import fr.gendarmerie.stsisi.adr.utilities.Item;

import static fr.gendarmerie.stsisi.adr.utilities.Utilities.firstCharToUpperCase;
import static fr.gendarmerie.stsisi.adr.utilities.Utilities.formatUnCode;

public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.SearchResultViewHolder> {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    private static final String TAG = ItemListAdapter.class.getSimpleName();

    class SearchResultViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private final TextView adapter_list_search_result_textView_kemler;
        private final TextView adapter_list_search_result_textView_unNumber;
        private final TextView adapter_list_search_result_textView_name;
        private final TextView adapter_list_search_result_textView_description;
        private final ImageView adapter_list_search_result_imageView_symbol_1,
                adapter_list_search_result_imageView_symbol_2,
                adapter_list_search_result_imageView_symbol_3,
                adapter_list_search_result_imageView_symbol_4;
        private final List<ImageView> symbolImageViewList = new ArrayList<>();

        private SearchResultViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            adapter_list_search_result_textView_kemler = itemView.findViewById(R.id.adapter_list_search_result_textView_kemler);
            adapter_list_search_result_textView_unNumber = itemView.findViewById(R.id.adapter_list_search_result_textView_unNumber);
            adapter_list_search_result_textView_name = itemView.findViewById(R.id.adapter_list_search_result_textView_name);
            adapter_list_search_result_textView_description = itemView.findViewById(R.id.adapter_list_search_result_textView_description);
            adapter_list_search_result_imageView_symbol_1 = itemView.findViewById(R.id.adapter_list_search_result_imageView_symbol_1);
            adapter_list_search_result_imageView_symbol_2 = itemView.findViewById(R.id.adapter_list_search_result_imageView_symbol_2);
            adapter_list_search_result_imageView_symbol_3 = itemView.findViewById(R.id.adapter_list_search_result_imageView_symbol_3);
            adapter_list_search_result_imageView_symbol_4 = itemView.findViewById(R.id.adapter_list_search_result_imageView_symbol_4);
            symbolImageViewList.add(adapter_list_search_result_imageView_symbol_1);
            symbolImageViewList.add(adapter_list_search_result_imageView_symbol_2);
            symbolImageViewList.add(adapter_list_search_result_imageView_symbol_3);
            symbolImageViewList.add(adapter_list_search_result_imageView_symbol_4);
        }

        @Override
        public void onClick(View v) {
            mListener.onClick(getAdapterPosition());
        }
    }

    private final Context mContext;
    private final Resources mResources;
    private final List<Item> mEntities;
    private ItemListAdapter.SearchResultViewHolder mHolder;
    private final RecyclerViewClickListener mListener;

    public ItemListAdapter(@NonNull Context context, @NonNull List<Item> entities, RecyclerViewClickListener listener) {
        mContext = context;
        mEntities = entities;
        mResources = context.getResources();
        mListener = listener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public @NonNull ItemListAdapter.SearchResultViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_list_search_result, parent, false);
        return new ItemListAdapter.SearchResultViewHolder(itemView);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull ItemListAdapter.SearchResultViewHolder holder, int position) {
        mHolder = holder;
        Item currentItem = mEntities.get(position);

        holder.adapter_list_search_result_textView_kemler.setText(currentItem.getKemler()!=null? currentItem.getKemler().getCode():"");
        holder.adapter_list_search_result_textView_unNumber.setText(formatUnCode(currentItem.getMaterial().getUnCode().toString()));
        holder.adapter_list_search_result_textView_name.setText(firstCharToUpperCase(currentItem.getName().getNameFr()));

        String description = mResources.getString(R.string.category)+" "+ currentItem.getCategory().getCode();
        if(currentItem.getGroup()!=null){description+=", "+ currentItem.getGroup().getCode();}
        if(currentItem.getTunnel()!=null){description+=", "+ currentItem.getTunnel().getCode();}

        holder.adapter_list_search_result_textView_description.setText(description);

        clearSymbol();
        setSymbol(currentItem);
    }

    // getItemCount() is called many times, and when it is first called,
    // mEntities has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mEntities != null)
            return mEntities.size();
        else return 0;
    }

    private void clearSymbol(){
        int id = mResources.getIdentifier("ic_adr_1", "drawable", mContext.getPackageName());
        if(id!=0){
            //Drawable drawable = mResources.getDrawable(id);
            for (ImageView view: mHolder.symbolImageViewList) {
                view.setImageResource(id);
                view.setVisibility(View.INVISIBLE);
            }
        }
        else{
            Log.e(TAG , "No drawable found with name " + "ic_adr_1");
        }
    }

    private void setSymbol(Item current) {
        int index = 0;
        if (!current.getSymbols().isEmpty()) {
            for (Symbol symbol : current.getSymbols()) {
                String url = symbol.getUri();
                int id = mResources.getIdentifier(url, "drawable", mContext.getPackageName());
                if (id != 0) {
                    mHolder.symbolImageViewList.get(index).setImageResource(id);
                    mHolder.symbolImageViewList.get(index++).setVisibility(View.VISIBLE);
                } else {
                    Log.e(TAG, "No drawable found with name " + url);
                }
            }
        }
    }
}

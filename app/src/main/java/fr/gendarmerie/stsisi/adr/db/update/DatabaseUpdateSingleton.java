package fr.gendarmerie.stsisi.adr.db.update;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import fr.gendarmerie.stsisi.adr.db.Entity;
import fr.gendarmerie.stsisi.adr.db.entities.Category;
import fr.gendarmerie.stsisi.adr.db.entities.Classification;
import fr.gendarmerie.stsisi.adr.db.entities.Country;
import fr.gendarmerie.stsisi.adr.db.entities.Group;
import fr.gendarmerie.stsisi.adr.db.entities.HasSymbol;
import fr.gendarmerie.stsisi.adr.db.entities.Hazard;
import fr.gendarmerie.stsisi.adr.db.entities.Kemler;
import fr.gendarmerie.stsisi.adr.db.entities.Material;
import fr.gendarmerie.stsisi.adr.db.entities.Name;
import fr.gendarmerie.stsisi.adr.db.entities.State;
import fr.gendarmerie.stsisi.adr.db.entities.Symbol;
import fr.gendarmerie.stsisi.adr.db.entities.Tunnel;
import fr.gendarmerie.stsisi.adr.db.entities.Type;
import fr.gendarmerie.stsisi.adr.interfaces.db.DatabaseInsertStateNotifier;
import fr.gendarmerie.stsisi.adr.interfaces.db.DatabaseUpdateProgressNotifier;
import fr.gendarmerie.stsisi.adr.interfaces.db.DatabaseUpdateStateNotifier;
import fr.gendarmerie.stsisi.adr.interfaces.db.DatabaseVersionCheckNotifier;
import fr.gendarmerie.stsisi.adr.repository.LocalRepository;
import fr.gendarmerie.stsisi.adr.utilities.App;
import fr.gendarmerie.stsisi.adr.utilities.VolleySingleton;
import fr.gendarmerie.stsisi.adr.interfaces.network.VolleyResultNotifier;

import static fr.gendarmerie.stsisi.adr.utilities.ConstantsKt.APPLICATION_SHARED_PREFERENCES;
import static fr.gendarmerie.stsisi.adr.utilities.ConstantsKt.APPLICATION_SHARED_PREFERENCES_VERSION_DATE;
import static fr.gendarmerie.stsisi.adr.utilities.ConstantsKt.APPLICATION_SHARED_PREFERENCES_VERSION_NUMBER;
import static fr.gendarmerie.stsisi.adr.utilities.ConstantsKt.DATABASE_SERVER_TIMESTAMP_FIELD;
import static fr.gendarmerie.stsisi.adr.utilities.ConstantsKt.DATABASE_SERVER_UPDATE_URI;
import static fr.gendarmerie.stsisi.adr.utilities.ConstantsKt.DATABASE_SERVER_VERSION_FIELD;
import static fr.gendarmerie.stsisi.adr.utilities.ConstantsKt.DATABASE_SERVER_VERSION_URI;

/**
 * This class is used as a manager for local database updates, uses the Volley
 * library to communicate with distant server that holds reference database.
 * HTTP requests are answered into JSON format. These JSON objects are hydrated
 * into different entity objects, before been inserted into local database.
 *
 * This class uses singleton pattern to insure that only one update can occur
 * at the same time.
 *
 * Notification of different events is made through interface mechanism.
 * Classes, that needs to received these events, have to register themselves
 * as callback objects, through methods "add*EVENT*Callback". When a class
 * doesn't need to received callback, it has to unregister itself, through
 * methods "remove*EVENT*Callback".
 *
 * Once you get an instance of this singleton, you can check version of the
 * distant reference database. A request is send, and when the server answers
 * notifyVersionReceived method is called. Depending on the difference between
 * database versions, notifications are send to callback objects through
 * interfaces DatabaseVersionCheckNotifier and DatabaseUpdateStateNotifier.
 *
 * Then with update method you can request an update from the distant reference
 * database. A request is send, and when the server answers
 * notifyJSONObjectReceived method is called, it calls parseJSONObjectUpdate
 * method to parse
 *
 * callback
 *
 *
 *
 *
 * @author Rémy PERSUANNE
 * @version 1.0
 */
public class DatabaseUpdateSingleton implements VolleyResultNotifier, DatabaseInsertStateNotifier {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to
     * identify it.
     */
    private static final String TAG = DatabaseUpdateSingleton.class.getSimpleName();

    /**
     * @value #STATIC_FIELD entities list is an array of table's name, the order
     * is important because this order is used to insert data in database, if you
     * change it, take care to respect foreign keys constraints.
     */
    private static final List<String> CONTAINERS_LIST = new ArrayList<>(Arrays.asList("add", "update", "delete"));

    /**
     * @value #STATIC_FIELD entities list is an array of table's name, the order
     * is important because this order is used to insert data in database,
     * if you change it, take care to respect foreign keys constraints.
     */
    private static final List<String> ENTITIES_LIST = new ArrayList<>(Arrays.asList("category", "country", "group", "hazard", "kemler", "name", "state", "symbol", "tunnel", "type", "classification", "material", "hasSymbol"));

    private static DatabaseUpdateSingleton mInstance;
    private static final AtomicBoolean isUpdateRunning = new AtomicBoolean(false);
    private static final AtomicInteger mNbEntityProcessed = new AtomicInteger(0);
    private static final AtomicInteger mNbElementsToProcess = new AtomicInteger(0);

    private static DatabaseVersion mClientDatabaseVersion,
                            mServerDatabaseVersion;

    private final String mDatabaseServerURL;
    private final VolleySingleton mVolleySingleton;
    private SharedPreferences mSharedPreferences;
    private final Application mApplication;
    private final List<DatabaseUpdateStateNotifier> mStateCallBacks = new ArrayList<>();
    private final List<DatabaseUpdateProgressNotifier> mProgressCallBacks = new ArrayList<>();
    private final List<DatabaseVersionCheckNotifier> mVersionCallBacks = new ArrayList<>();


    /**
     * Default constructor.
     * Use constant value DATABASE_SERVER_URL store in utilities.ConstantsKt
     * as default URL to communicate with database
     */
    private DatabaseUpdateSingleton(Application application){
        this.mApplication = application;
        this.mDatabaseServerURL = fr.gendarmerie.stsisi.adr.utilities.ConstantsKt.DATABASE_SERVER_URL;
        this.mVolleySingleton = VolleySingleton.getInstance(application, this);
        init();
    }

    public static synchronized DatabaseUpdateSingleton getInstance(){
        return mInstance;
    }


    public static synchronized DatabaseUpdateSingleton getInstance(Application application){
        if (mInstance == null) {
            mInstance = new DatabaseUpdateSingleton(application);
        }

        return mInstance;
    }

    public Boolean isLocalDatabaseExist(){
        return mSharedPreferences.contains(APPLICATION_SHARED_PREFERENCES_VERSION_NUMBER) && mSharedPreferences.contains(APPLICATION_SHARED_PREFERENCES_VERSION_DATE);
    }

    public Boolean isUpdateRunning(){
        return isUpdateRunning.get();
    }

    private void init(){
        mSharedPreferences = mApplication.getSharedPreferences(APPLICATION_SHARED_PREFERENCES,Context.MODE_PRIVATE);
        mClientDatabaseVersion = getClientDatabaseVersion();
        DatabaseUpdateSingleton.isUpdateRunning.set(false);
    }

    public void addStateCallback(DatabaseUpdateStateNotifier callBack){
        mStateCallBacks.add(callBack);
    }

    public void addProgressCallback(DatabaseUpdateProgressNotifier callBack){
        mProgressCallBacks.add(callBack);
    }

    public void addVersionCallback(DatabaseVersionCheckNotifier callBack){
        mVersionCallBacks.add(callBack);
    }

    public void removeStateCallback(DatabaseUpdateStateNotifier callBack){
        mStateCallBacks.remove(callBack);
    }

    public void removeProgressCallback(DatabaseUpdateProgressNotifier callBack){
        mProgressCallBacks.remove(callBack);
    }

    public void removeVersionCallback(DatabaseVersionCheckNotifier callBack){
        mVersionCallBacks.remove(callBack);
    }

    @Override
    public void notifyVersionReceived(JSONObject response){
        this.parseJSONObjectVersion(response);
    }

    @Override
    public void notifyJsonObjectReceived(JSONObject response) {
        new DatabaseUpdateSingleton.parseJSONObjectUpdate().execute(response);
    }

    @Override
    public void notifyNetworkError(String requestType, VolleyError error) {
        Log.e(TAG,requestType+" request from "+mDatabaseServerURL+" is incorrect : "+error.getMessage());
        for(DatabaseUpdateStateNotifier callback : mStateCallBacks) {
            callback.notifyUnknown();
        }
    }

    /**
     * Check if the local database is up to date.
     * The function compare the timestamp of the last update of the local database with that
     * of the last update of database server.
     * If the local time stamp is less than that of the server, the database is not up to date.
     */
    public void update(){
        if(!isUpdateRunning.get()){
            isUpdateRunning.set(true);
            String URL = DATABASE_SERVER_UPDATE_URI;
            URL += DatabaseUpdateSingleton.mClientDatabaseVersion!=null?DATABASE_SERVER_VERSION_FIELD+DatabaseUpdateSingleton.mClientDatabaseVersion.getVersionNumber()+DATABASE_SERVER_TIMESTAMP_FIELD+DatabaseUpdateSingleton.mClientDatabaseVersion.getVersionDateString():"";
            DatabaseUpdateSingleton.mNbEntityProcessed.set(0);
            mVolleySingleton.sendGetJsonObjectRequest("GET UPDATE",URL);
        }
        else{
            Log.w(TAG, "Update already in progress");
        }
    }

    public void deleteAll(){
        // Ask repository to delete all tables in the correct order to avoid conflict with foreign key constraint
        App.getLocalRepository().deleteAll();
        //
        deleteClientDatabaseVersion();
    }

    public void checkVersion(){
        getServerDatabaseVersion();
    }

    public synchronized DatabaseVersion getClientDatabaseVersion(){
        if(!mSharedPreferences.contains(APPLICATION_SHARED_PREFERENCES_VERSION_NUMBER) || !mSharedPreferences.contains(APPLICATION_SHARED_PREFERENCES_VERSION_DATE)){
            return null;
        }

        return new DatabaseVersion(mSharedPreferences.getString(APPLICATION_SHARED_PREFERENCES_VERSION_NUMBER,null), mSharedPreferences.getString(APPLICATION_SHARED_PREFERENCES_VERSION_DATE, null));
    }

    private void deleteClientDatabaseVersion(){
        mSharedPreferences
                .edit()
                .remove(APPLICATION_SHARED_PREFERENCES_VERSION_NUMBER)
                .remove(APPLICATION_SHARED_PREFERENCES_VERSION_DATE)
                .apply();
        mClientDatabaseVersion=null;
    }

    private synchronized void setClientDatabaseVersion(DatabaseVersion version){
        mSharedPreferences
                .edit()
                .putString(APPLICATION_SHARED_PREFERENCES_VERSION_NUMBER, version.getVersionNumber())
                .putString(APPLICATION_SHARED_PREFERENCES_VERSION_DATE, version.getVersionDateString())
                .apply();
        mClientDatabaseVersion=version;
    }

    private void getServerDatabaseVersion(){
        mVolleySingleton.sendGetJsonObjectRequest("GET VERSION", DATABASE_SERVER_VERSION_URI);
    }

    private void parseJSONObjectVersion(JSONObject container){
        try {
            JSONObject jsonVersion = container.getJSONObject("version");
            String versionNumber = jsonVersion.getString("number");
            String versionDate = jsonVersion.getString("date");
            mServerDatabaseVersion = new DatabaseVersion(versionNumber, versionDate);

            for(DatabaseUpdateStateNotifier callback : mStateCallBacks) {
                if(mClientDatabaseVersion!=null && mClientDatabaseVersion.isUptoDate(mServerDatabaseVersion)){
                    callback.notifyUpToDate();
                }
                else{
                    callback.notifyUpdate();
                }
            }

            for(DatabaseVersionCheckNotifier callback : mVersionCallBacks) {
                callback.notifyVersion(getClientDatabaseVersion());
            }
        }
        catch(JSONException exception){

            Log.e(TAG, "parseJSONObjectVersion : "+exception.getCause()+" "+exception.getMessage());
        }
    }

    private static void notifyNbEntityMax(JSONObject jsonObject) {

        mNbElementsToProcess.set(0);

        try {
            for (String container: CONTAINERS_LIST) {
                for (String entity: ENTITIES_LIST) {
                    if(jsonObject.optJSONObject(container)!=null && jsonObject.getJSONObject(container).optJSONArray(entity)!=null)
                        mNbElementsToProcess.getAndAdd(jsonObject.getJSONObject(container).getJSONArray(entity).length());
                }
            }

        }
        catch(JSONException exception){
            Log.e(TAG, "notifyNbEntityMax : "+exception.getCause()+" "+exception.getMessage());
        }
    }



    public void notifyDatabaseInsertionSuccess(Entity entity) {
        DatabaseUpdateSingleton.mNbEntityProcessed.getAndAdd(1);

        for(DatabaseUpdateProgressNotifier callback : mProgressCallBacks) {
            callback.notifyProcessedEntity(entity);
            callback.notifyProgress(DatabaseUpdateSingleton.mNbEntityProcessed.get(), DatabaseUpdateSingleton.mNbElementsToProcess.get());

        }

        //Update finished
        if (DatabaseUpdateSingleton.mNbEntityProcessed.get()==DatabaseUpdateSingleton.mNbElementsToProcess.get()) {
            //Record local database version
            setClientDatabaseVersion(mServerDatabaseVersion);
            //Notify update finished
            for(DatabaseUpdateProgressNotifier callback : mProgressCallBacks) {
                callback.notifyUpdateFinished();
            }
            //Notify local database version
            for(DatabaseVersionCheckNotifier callback : mVersionCallBacks) {
                callback.notifyVersion(getClientDatabaseVersion());
            }
            isUpdateRunning.set(false);
        }
    }

    private static class parseJSONObjectUpdate extends AsyncTask<JSONObject, Void, Void> {

        @Override
        protected Void doInBackground(JSONObject... containers) {

            notifyNbEntityMax(containers[0]);

            try {
                // Version
                JSONObject jsonVersion = containers[0].getJSONObject("version");
                String versionNumber = jsonVersion.getString("number");
                String versionDate = jsonVersion.getString("date");

                mServerDatabaseVersion = new DatabaseVersion(versionNumber, versionDate);

                // Add elements
                JSONObject elementsToAdd = containers[0].getJSONObject("add");

                for (String entity: ENTITIES_LIST) {
                    insertJSONArrayToDB(entity, elementsToAdd.optJSONArray(entity));
                }

                // Update elements
                JSONObject elementsToUpdate = containers[0].getJSONObject("update");

                for (String entity: ENTITIES_LIST) {
                    insertJSONArrayToDB(entity, elementsToUpdate.optJSONArray(entity));
                }

                // Delete elements
                //JSONObject elementsToDelete = containers[0].getJSONObject("delete");
            }
            catch(JSONException exception){
                Log.e(TAG, "parseJSONObjectUpdate : "+exception.getCause()+" "+exception.getMessage());
            }

            return null;
        }
    }

    private static void insertJSONArrayToDB(String tableName, JSONArray jsonArray){
        if(jsonArray != null) {
            try {
                for (int i = 0; i < jsonArray.length(); i++) {
                    Log.d(TAG, "insertJSONArrayToDB : i=" + i + " table=" + tableName + " json=" + jsonArray.getJSONObject(i));
                    switch (tableName) {
                        case "category":
                            App.getLocalRepository().insert(new Category(jsonArray.getJSONObject(i)));
                            break;
                        case "classification":
                            App.getLocalRepository().insert(new Classification(jsonArray.getJSONObject(i)));
                            break;
                        case "country":
                            App.getLocalRepository().insert(new Country(jsonArray.getJSONObject(i)));
                            break;
                        case "group":
                            App.getLocalRepository().insert(new Group(jsonArray.getJSONObject(i)));
                            break;
                        case "hasSymbol":
                            App.getLocalRepository().insert(new HasSymbol(jsonArray.getJSONObject(i)));
                            break;
                        case "hazard":
                            App.getLocalRepository().insert(new Hazard(jsonArray.getJSONObject(i)));
                            break;
                        case "kemler":
                            App.getLocalRepository().insert(new Kemler(jsonArray.getJSONObject(i)));
                            break;
                        case "material":
                            App.getLocalRepository().insert(new Material(jsonArray.getJSONObject(i)));
                            break;
                        case "name":
                            App.getLocalRepository().insert(new Name(jsonArray.getJSONObject(i)));
                            break;
                        case "state":
                            App.getLocalRepository().insert(new State(jsonArray.getJSONObject(i)));
                            break;
                        case "symbol":
                            App.getLocalRepository().insert(new Symbol(jsonArray.getJSONObject(i)));
                            break;
                        case "tunnel":
                            App.getLocalRepository().insert(new Tunnel(jsonArray.getJSONObject(i)));
                            break;
                        case "type":
                            App.getLocalRepository().insert(new Type(jsonArray.getJSONObject(i)));
                            break;
                    }
                }
            }
            catch(JSONException exception){

                Log.e(TAG, "insertJSONArrayToDB : "+exception.getCause()+" "+exception.getMessage());
            }
        }
        else{
            Log.w(TAG, "insertJSONArrayToDB : void JSONArray");
        }
    }
}
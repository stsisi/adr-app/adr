package fr.gendarmerie.stsisi.adr.ui.fragment.unCode;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import fr.gendarmerie.stsisi.adr.R;
import fr.gendarmerie.stsisi.adr.db.entities.Symbol;
import fr.gendarmerie.stsisi.adr.interfaces.layout.LayoutHeaderChange;
import fr.gendarmerie.stsisi.adr.ui.fragment.symbol.SymbolFragment;
import fr.gendarmerie.stsisi.adr.utilities.Item;

import static fr.gendarmerie.stsisi.adr.utilities.Utilities.firstCharToUpperCase;
import static fr.gendarmerie.stsisi.adr.utilities.Utilities.formatUnCode;
import static fr.gendarmerie.stsisi.adr.utilities.Utilities.toTitleCase;

public class EntityFragment extends Fragment {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    private static final String TAG = EntityFragment.class.getSimpleName();

    private Activity mParentActivity;
    private LayoutHeaderChange mActivityCallback;
    private Context mAppContext;
    private View mView;
    private Resources mResources;
    private Item mItem;
    private final List<ImageView> symbolImageViewList = new ArrayList<>();

    private TextView fragment_entity_textView_material_name;
    private TextView fragment_entity_plate_textView_kemler;
    private TextView fragment_entity_plate_textView_unNumber;
    private TextView fragment_entity_textView_label_kemler;
    private TextView fragment_entity_textView_kemler;
    private TextView fragment_entity_textView_label_classification;
    private TextView fragment_entity_textView_classification;
    private TextView fragment_entity_textView_label_type;
    private TextView fragment_entity_textView_type;
    private TextView fragment_entity_textView_label_hazard;
    private TextView fragment_entity_textView_hazard;
    private TextView fragment_entity_textView_label_state;
    private TextView fragment_entity_textView_state;
    private TextView fragment_entity_textView_category;
    private TextView fragment_entity_textView_label_group;
    private TextView fragment_entity_textView_group;
    private TextView fragment_entity_textView_label_tunnel;
    private TextView fragment_entity_textView_tunnel;

    private final View.OnClickListener symbolListener = v -> {

        String symbolTag = v.getTag().toString();
        Fragment fragment = new SymbolFragment();

        Bundle args = new Bundle();
        args.putString("symbolTag", symbolTag);
        fragment.setArguments(args);

        FragmentTransaction fragmentTransaction = Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.activity_main_fragment_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mAppContext = context.getApplicationContext();

        if (context instanceof Activity) {
            mParentActivity = (Activity) context;

            try {
                mActivityCallback = (LayoutHeaderChange) mParentActivity;
            } catch (ClassCastException e) {
                throw new ClassCastException(mParentActivity.toString()
                        + " must implement LayoutHeaderChange");
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Set fragment attribute and inflate layout
        mView = inflater.inflate(R.layout.fragment_entity, container, false);

        mResources = mParentActivity.getResources();

        // Change layout layout_portrait_header properties through MainActivity's interface LayoutHeaderChange
        mActivityCallback.showButtonBack();
        mActivityCallback.hideLogo();
        mActivityCallback.changeTitle("Matière Transportée");

        Bundle args = getArguments();
        assert args != null;
        mItem = (Item) args.getSerializable("searchResult");

        this.getViews();

        this.setViews();

        return mView;
    }

    private void getViews() {
        fragment_entity_textView_material_name = mView.findViewById(R.id.fragment_entity_textView_material_name);
        fragment_entity_plate_textView_kemler = mView.findViewById(R.id.fragment_entity_plate_textView_kemler);
        fragment_entity_plate_textView_unNumber = mView.findViewById(R.id.fragment_entity_plate_textView_unNumber);
        fragment_entity_textView_label_kemler = mView.findViewById(R.id.fragment_entity_textView_label_kemler);
        fragment_entity_textView_kemler = mView.findViewById(R.id.fragment_entity_textView_kemler);
        fragment_entity_textView_label_classification = mView.findViewById(R.id.fragment_entity_textView_label_classification);
        fragment_entity_textView_classification = mView.findViewById(R.id.fragment_entity_textView_classification);
        fragment_entity_textView_label_type = mView.findViewById(R.id.fragment_entity_textView_label_type);
        fragment_entity_textView_type = mView.findViewById(R.id.fragment_entity_textView_type);
        fragment_entity_textView_label_hazard = mView.findViewById(R.id.fragment_entity_textView_label_hazard);
        fragment_entity_textView_hazard = mView.findViewById(R.id.fragment_entity_textView_hazard);
        fragment_entity_textView_label_state = mView.findViewById(R.id.fragment_entity_textView_label_state);
        fragment_entity_textView_state = mView.findViewById(R.id.fragment_entity_textView_state);
        fragment_entity_textView_category = mView.findViewById(R.id.fragment_entity_textView_category);
        fragment_entity_textView_label_group = mView.findViewById(R.id.fragment_entity_textView_label_group);
        fragment_entity_textView_group = mView.findViewById(R.id.fragment_entity_textView_group);
        fragment_entity_textView_label_tunnel = mView.findViewById(R.id.fragment_entity_textView_label_tunnel);
        fragment_entity_textView_tunnel = mView.findViewById(R.id.fragment_entity_textView_tunnel);

        ImageView fragment_entity_imageView_symbol_1 = mView.findViewById(R.id.fragment_entity_imageView_symbol_1);
        ImageView fragment_entity_imageView_symbol_2 = mView.findViewById(R.id.fragment_entity_imageView_symbol_2);
        ImageView fragment_entity_imageView_symbol_3 = mView.findViewById(R.id.fragment_entity_imageView_symbol_3);
        ImageView fragment_entity_imageView_symbol_4 = mView.findViewById(R.id.fragment_entity_imageView_symbol_4);

        symbolImageViewList.clear();

        symbolImageViewList.add(fragment_entity_imageView_symbol_1);
        symbolImageViewList.add(fragment_entity_imageView_symbol_2);
        symbolImageViewList.add(fragment_entity_imageView_symbol_3);
        symbolImageViewList.add(fragment_entity_imageView_symbol_4);

    }

    private void setViews() {
        fragment_entity_textView_material_name.setText(toTitleCase(mItem.getName().getNameFr()));
        fragment_entity_plate_textView_kemler.setText(mItem.getKemler()!=null? mItem.getKemler().getCode():"");
        fragment_entity_plate_textView_unNumber.setText(formatUnCode(mItem.getMaterial().getUnCode().toString()));

        if(mItem.getKemler()!=null) {
            fragment_entity_textView_kemler.setText(firstCharToUpperCase(mItem.getKemler().getName()));
        }
        else{
            fragment_entity_textView_label_kemler.setVisibility(View.GONE);
            fragment_entity_textView_kemler.setVisibility(View.GONE);
        }

        if(mItem.getClassification()!=null) {
            fragment_entity_textView_classification.setText(firstCharToUpperCase(mItem.getType().getCode()+ mItem.getState().getCode()+ mItem.getHazard().getCode()));
            fragment_entity_textView_type.setText(firstCharToUpperCase(mItem.getType().getName()+" "+ mItem.getType().getDescription()));
            if(mItem.getHazard().getName().isEmpty() && mItem.getHazard().getComment().isEmpty()){
                fragment_entity_textView_label_hazard.setVisibility(View.GONE);
                fragment_entity_textView_hazard.setVisibility(View.GONE);
            }
            else{
                fragment_entity_textView_hazard.setText(mItem.getHazard().getName().isEmpty()?"":firstCharToUpperCase(mItem.getHazard().getName()+(mItem.getHazard().getComment().isEmpty()?"":", "+ mItem.getHazard().getComment())));
            }

            if(mItem.getState().getName().isEmpty() && mItem.getState().getComment().isEmpty()){
                fragment_entity_textView_label_state.setVisibility(View.GONE);
                fragment_entity_textView_state.setVisibility(View.GONE);
            }
            else{
                fragment_entity_textView_state.setText(firstCharToUpperCase(mItem.getState().getName().isEmpty() ? mItem.getState().getComment() : mItem.getState().getName()+" "+ mItem.getState().getComment()));
            }
        }
        else{
            fragment_entity_textView_label_classification.setVisibility(View.GONE);
            fragment_entity_textView_classification.setVisibility(View.GONE);
            fragment_entity_textView_label_type.setVisibility(View.GONE);
            fragment_entity_textView_type.setVisibility(View.GONE);
            fragment_entity_textView_label_hazard.setVisibility(View.GONE);
            fragment_entity_textView_hazard.setVisibility(View.GONE);
            fragment_entity_textView_label_state.setVisibility(View.GONE);
            fragment_entity_textView_state.setVisibility(View.GONE);
        }

        fragment_entity_textView_category.setText(mAppContext.getString(R.string.fragment_entity_category, mItem.getCategory().getCode(), firstCharToUpperCase(mItem.getCategory().getName())));

        if(mItem.getGroup()!=null) {
            fragment_entity_textView_group.setText(firstCharToUpperCase(mItem.getGroup().getCode() + ", " + mItem.getGroup().getDescription() + ", " + mItem.getGroup().getComment()));
        }
        else{
            fragment_entity_textView_label_group.setVisibility(View.GONE);
            fragment_entity_textView_group.setVisibility(View.GONE);
        }

        if(mItem.getTunnel()!=null) {
            fragment_entity_textView_tunnel.setText(firstCharToUpperCase(mItem.getTunnel().getCode()));
        }
        else{
            fragment_entity_textView_label_tunnel.setVisibility(View.GONE);
            fragment_entity_textView_tunnel.setVisibility(View.GONE);
        }

        clearSymbol();
        setSymbol(mItem);
    }

    private void clearSymbol(){
        int id = mResources.getIdentifier("ic_adr_1", "drawable", mAppContext.getPackageName());
        if(id!=0){
            Drawable drawable = mResources.getDrawable(id);
            for (ImageView view: symbolImageViewList) {
                view.setImageDrawable(drawable);
                view.setVisibility(View.INVISIBLE);
            }
        }
        else{
            Log.e(TAG , "No drawable found with name " + "ic_adr_1");
        }
    }

    private void setSymbol(Item current) {
        int index = 0;
        if (!current.getSymbols().isEmpty()) {
            for (Symbol symbol : current.getSymbols()) {
                String url = symbol.getUri();
                int id = mResources.getIdentifier(url, "drawable", mAppContext.getPackageName());
                if (id != 0) {
                    Drawable drawable = mResources.getDrawable(id);
                    symbolImageViewList.get(index).setImageDrawable(drawable);
                    symbolImageViewList.get(index).invalidateDrawable(drawable);
                    symbolImageViewList.get(index).setVisibility(View.VISIBLE);
                    symbolImageViewList.get(index).setTag(symbol.getUri());
                    symbolImageViewList.get(index++).setOnClickListener(symbolListener);
                } else {
                    Log.e(TAG, "No drawable found with name " + url);
                }
            }
        }
    }
}
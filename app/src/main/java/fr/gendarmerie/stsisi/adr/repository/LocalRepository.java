package fr.gendarmerie.stsisi.adr.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.gendarmerie.stsisi.adr.db.ADRRoomDatabase;
import fr.gendarmerie.stsisi.adr.db.dao.Dao;
import fr.gendarmerie.stsisi.adr.db.entities.Category;
import fr.gendarmerie.stsisi.adr.db.entities.Classification;
import fr.gendarmerie.stsisi.adr.db.entities.Country;
import fr.gendarmerie.stsisi.adr.db.entities.Group;
import fr.gendarmerie.stsisi.adr.db.entities.HasSymbol;
import fr.gendarmerie.stsisi.adr.db.entities.Hazard;
import fr.gendarmerie.stsisi.adr.db.entities.Kemler;
import fr.gendarmerie.stsisi.adr.db.entities.Material;
import fr.gendarmerie.stsisi.adr.db.entities.Name;
import fr.gendarmerie.stsisi.adr.db.entities.State;
import fr.gendarmerie.stsisi.adr.db.entities.Symbol;
import fr.gendarmerie.stsisi.adr.db.entities.Tunnel;
import fr.gendarmerie.stsisi.adr.db.entities.Type;
import fr.gendarmerie.stsisi.adr.db.update.DatabaseUpdateSingleton;

public class LocalRepository {
    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    private static final String TAG = LocalRepository.class.getSimpleName();

    /**
     * @value #STATIC_FIELD entities list is an array of table's name, the order is important
     * because this order is used to insert data in database, if you change it, take care to respect
     * foreign keys constraints.
     */
    private static final List<String> DELETE_ORDER_LIST = new ArrayList<>(Arrays.asList("hasSymbol", "material", "classification", "group", "type", "category", "country",  "hazard", "kemler", "name", "state", "symbol", "tunnel"));


    private final Dao mDao;

    public LocalRepository(Application application){
        ADRRoomDatabase mDatabase = ADRRoomDatabase.getDatabase(application);
        this.mDao = mDatabase.dao();
    }

    public LiveData<List<Country>> getAllCountry() {
        return mDao.getAllCountry();
    }

    public List<Symbol> getSymbolByUri(String uri) {
        return mDao.getSymbolByUri(uri);
    }

    public List<Material> getMaterialBySymbol(String url) {
        return mDao.getMaterialBySymbol(url);
    }

    public List<Material> getMaterialByUnCode(int code) {
        return mDao.getMaterialByCode(code);
    }

    public List<Material> getMaterialByUnCode(int code, String kemler) {
        return mDao.getMaterialByCode(code, kemler);
    }

    public List<Material> getMaterialByKemlerCode(String kemler) {
        return mDao.getMaterialByCode(kemler);
    }

    public int getFirstUnCode() {
        return mDao.getFirstUnCode();
    }

    public int getLastUnCode() {
        return mDao.getLastUnCode();
    }

    public int getNextUnCode(int current) {
        return mDao.getNextUnCode(current);
    }

    public int getPreviousUnCode(int current) {
        return mDao.getPreviousUnCode(current);
    }

    public Category getCategoryByMaterial(int id) {
        return mDao.getCategoryByMaterial(id);
    }

    public Classification getClassificationByMaterial(int id) {
        return mDao.getClassificationByMaterial(id);
    }

    public Group getGroupByMaterial(int id) {
        return mDao.getGroupByMaterial(id);
    }

    public Hazard getHazardByClassification(int id) {
        return mDao.getHazardByClassification(id);
    }

    public Kemler getKemlerByMaterial(int id) {
        return mDao.getKemlerByMaterial(id);
    }

    public Name getNameByMaterial(int id) {
        return mDao.getNameByMaterial(id);
    }

    public List<Symbol> getAllSymbolByMaterial(int id) {
        return mDao.getAllSymbolByMaterial(id);
    }

    public State getStateByClassification(int id) {
        return mDao.getStateByClassification(id);
    }

    public Tunnel getTunnelByMaterial(int id) {
        return mDao.getTunnelByMaterial(id);
    }

    public Type getTypeByClassification(int id) {
        return mDao.getTypeByClassification(id);
    }

    public void deleteAll() {
        Log.w(TAG, "/!\\ delete data in all tables - PENDING /!\\");

        AsyncTask.execute(() -> {
            for (String table: DELETE_ORDER_LIST) {
                switch(table){
                    case "category":
                        mDao.deleteAllCategory();
                        break;
                    case "classification":
                        mDao.deleteAllClassification();
                        break;
                    case "country":
                        mDao.deleteAllCountry();
                        break;
                    case "group":
                        mDao.deleteAllGroup();
                        break;
                    case "hasSymbol":
                        mDao.deleteAllHasSymbol();
                        break;
                    case "hazard":
                        mDao.deleteAllHazard();
                        break;
                    case "kemler":
                        mDao.deleteAllKemler();
                        break;
                    case "material":
                        mDao.deleteAllMaterial();
                        break;
                    case "name":
                        mDao.deleteAllName();
                        break;
                    case "state":
                        mDao.deleteAllState();
                        break;
                    case "symbol":
                        mDao.deleteAllSymbol();
                        break;
                    case "tunnel":
                        mDao.deleteAllTunnel();
                        break;
                    case "type":
                        mDao.deleteAllType();
                        break;
                }
            }

            Log.w(TAG, "/!\\ delete data in all tables - DONE /!\\");
        });
    }

    public void insert (Category entity) {
        Log.d(TAG, "insert category: entity="+entity);
        new LocalRepository.insertCategoryAsyncTask(mDao).execute(entity);
    }

    public void insert (Classification entity) {
        Log.d(TAG, "insert classification: entity="+entity);
        new LocalRepository.insertClassificationAsyncTask(mDao).execute(entity);
    }

    public void insert (Country entity) {
        Log.d(TAG, "insert country: entity="+entity);
        new LocalRepository.insertCountryAsyncTask(mDao).execute(entity);
    }

    public void insert (Group entity) {
        Log.d(TAG, "insert group: entity="+entity);
        new LocalRepository.insertGroupAsyncTask(mDao).execute(entity);
    }

    public void insert (HasSymbol entity) {
        Log.d(TAG, "insert hasSymbol: entity="+entity);
        new LocalRepository.insertHasSymbolAsyncTask(mDao).execute(entity);
    }

    public void insert (Hazard entity) {
        Log.d(TAG, "insert hazard: entity="+entity);
        new LocalRepository.insertHazardAsyncTask(mDao).execute(entity);
    }

    public void insert (Kemler entity) {
        Log.d(TAG, "insert kemler: entity="+entity);
        new LocalRepository.insertKemlerAsyncTask(mDao).execute(entity);
    }

    public void insert (Material entity) {
        Log.i(TAG, "insert material: entity="+entity);
        new LocalRepository.insertMaterialAsyncTask(mDao).execute(entity);
    }

    public void insert (Name entity) {
        Log.d(TAG, "insert name: entity="+entity);
        new LocalRepository.insertNameAsyncTask(mDao).execute(entity);
    }

    public void insert (State entity) {
        Log.d(TAG, "insert state: entity="+entity);
        new LocalRepository.insertStateAsyncTask(mDao).execute(entity);
    }

    public void insert (Symbol entity) {
        Log.d(TAG, "insert symbol: entity="+entity);
        new LocalRepository.insertSymbolAsyncTask(mDao).execute(entity);
    }

    public void insert (Tunnel entity) {
        Log.d(TAG, "insert tunnel: entity="+entity);
        new LocalRepository.insertTunnelAsyncTask(mDao).execute(entity);
    }

    public void insert (Type entity) {
        Log.d(TAG, "insert type: entity="+entity);
        new LocalRepository.insertTypeAsyncTask(mDao).execute(entity);
    }

    private static class insertCategoryAsyncTask extends AsyncTask<Category, Void, Void> {
        private final Dao mAsyncTaskDao;
        insertCategoryAsyncTask(Dao dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(final Category... params) {
            Log.d(TAG, "insertCategoryAsyncTask : entity="+params[0]);
            mAsyncTaskDao.insertCategory(params[0]);
            DatabaseUpdateSingleton.getInstance().notifyDatabaseInsertionSuccess(params[0]);
            return null;
        }
    }

    private static class insertClassificationAsyncTask extends AsyncTask<Classification, Void, Void> {
        private final Dao mAsyncTaskDao;
        insertClassificationAsyncTask(Dao dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(final Classification... params) {
            Log.d(TAG, "insertClassificationAsyncTask : entity="+params[0]);
            mAsyncTaskDao.insertClassification(params[0]);
            DatabaseUpdateSingleton.getInstance().notifyDatabaseInsertionSuccess(params[0]);
            return null;
        }
    }

    private static class insertCountryAsyncTask extends AsyncTask<Country, Void, Void> {
        private final Dao mAsyncTaskDao;
        insertCountryAsyncTask(Dao dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(final Country... params) {
            Log.d(TAG, "insertCountryAsyncTask : entity="+params[0]);
            mAsyncTaskDao.insertCountry(params[0]);
            DatabaseUpdateSingleton.getInstance().notifyDatabaseInsertionSuccess(params[0]);
            return null;
        }
    }

    private static class insertGroupAsyncTask extends AsyncTask<Group, Void, Void> {
        private final Dao mAsyncTaskDao;
        insertGroupAsyncTask(Dao dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(final Group... params) {
            Log.d(TAG, "insertGroupAsyncTask : entity="+params[0]);
            mAsyncTaskDao.insertGroup(params[0]);
            DatabaseUpdateSingleton.getInstance().notifyDatabaseInsertionSuccess(params[0]);
            return null;
        }
    }

    private static class insertHasSymbolAsyncTask extends AsyncTask<HasSymbol, Void, Void> {
        private final Dao mAsyncTaskDao;
        insertHasSymbolAsyncTask(Dao dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(final HasSymbol... params) {
            Log.d(TAG, "insertHasSymbolAsyncTask : entity="+params[0]);
            mAsyncTaskDao.insertHasSymbol(params[0]);
            DatabaseUpdateSingleton.getInstance().notifyDatabaseInsertionSuccess(params[0]);
            return null;
        }
    }

    private static class insertHazardAsyncTask extends AsyncTask<Hazard, Void, Void> {
        private final Dao mAsyncTaskDao;
        insertHazardAsyncTask(Dao dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(final Hazard... params) {
            Log.d(TAG, "insertHazardAsyncTask : entity="+params[0]);
            mAsyncTaskDao.insertHazard(params[0]);
            DatabaseUpdateSingleton.getInstance().notifyDatabaseInsertionSuccess(params[0]);
            return null;
        }
    }

    private static class insertKemlerAsyncTask extends AsyncTask<Kemler, Void, Void> {
        private final Dao mAsyncTaskDao;
        insertKemlerAsyncTask(Dao dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(final Kemler... params) {
            Log.d(TAG, "insertKemlerAsyncTask : entity="+params[0]);
            mAsyncTaskDao.insertKemler(params[0]);
            DatabaseUpdateSingleton.getInstance().notifyDatabaseInsertionSuccess(params[0]);
            return null;
        }
    }

    private static class insertMaterialAsyncTask extends AsyncTask<Material, Void, Void> {
        private final Dao mAsyncTaskDao;
        insertMaterialAsyncTask(Dao dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(final Material... params) {
            Log.d(TAG, "insertMaterialAsyncTask : entity="+params[0].toString());
            mAsyncTaskDao.insertMaterial(params[0]);
            DatabaseUpdateSingleton.getInstance().notifyDatabaseInsertionSuccess(params[0]);
            return null;
        }
    }

    private static class insertNameAsyncTask extends AsyncTask<Name, Void, Void> {
        private final Dao mAsyncTaskDao;
        insertNameAsyncTask(Dao dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(final Name... params) {
            Log.d(TAG, "insertNameAsyncTask : entity="+params[0]);
            mAsyncTaskDao.insertName(params[0]);
            DatabaseUpdateSingleton.getInstance().notifyDatabaseInsertionSuccess(params[0]);
            return null;
        }
    }

    private static class insertStateAsyncTask extends AsyncTask<State, Void, Void> {
        private final Dao mAsyncTaskDao;
        insertStateAsyncTask(Dao dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(final State... params) {
            Log.d(TAG, "insertStateAsyncTask : entity="+params[0]);
            mAsyncTaskDao.insertState(params[0]);
            DatabaseUpdateSingleton.getInstance().notifyDatabaseInsertionSuccess(params[0]);
            return null;
        }
    }

    private static class insertSymbolAsyncTask extends AsyncTask<Symbol, Void, Void> {
        private final Dao mAsyncTaskDao;
        insertSymbolAsyncTask(Dao dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(final Symbol... params) {
            Log.d(TAG, "insertSymbolAsyncTask : entity="+params[0]);
            mAsyncTaskDao.insertSymbol(params[0]);
            DatabaseUpdateSingleton.getInstance().notifyDatabaseInsertionSuccess(params[0]);
            return null;
        }
    }

    private static class insertTunnelAsyncTask extends AsyncTask<Tunnel, Void, Void> {
        private final Dao mAsyncTaskDao;
        insertTunnelAsyncTask(Dao dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(final Tunnel... params) {
            Log.d(TAG, "insertTunnelAsyncTask : entity="+params[0]);
            mAsyncTaskDao.insertTunnel(params[0]);
            DatabaseUpdateSingleton.getInstance().notifyDatabaseInsertionSuccess(params[0]);
            return null;
        }
    }

    private static class insertTypeAsyncTask extends AsyncTask<Type, Void, Void> {
        private final Dao mAsyncTaskDao;
        insertTypeAsyncTask(Dao dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(final Type... params) {
            Log.d(TAG, "insertTypeAsyncTask : entity="+params[0]);
            mAsyncTaskDao.insertType(params[0]);
            DatabaseUpdateSingleton.getInstance().notifyDatabaseInsertionSuccess(params[0]);
            return null;
        }
    }
}

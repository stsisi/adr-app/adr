package fr.gendarmerie.stsisi.adr.utilities

/**
 * Shared preferences name for the application
 */
const val APPLICATION_SHARED_PREFERENCES = "fr.gendarmerie.stsisi.adr"

/**
 * Name of shared preferences containing current local database version number
 */
const val APPLICATION_SHARED_PREFERENCES_VERSION_NUMBER = "VERSION_NUMBER"

/**
 * Name of shared preferences containing current local database version date
 */
const val APPLICATION_SHARED_PREFERENCES_VERSION_DATE = "VERSION_DATE"

/**
 * Name of shared preferences that contain a boolean, true means that the disclaimer was accepted
 */
const val APPLICATION_SHARED_PREFERENCES_DISCLAIMER_DIALOG = "DISCLAIMER_DIALOG"

/**
 * Prefix of drawer use as logo in the layout header
 */
const val HEADER_DRAWABLE_PREFIX = "drawer_background_"

/**
 * Value of margin right when footer is expanded up
 */
const val FOOTER_CONTAINER_MARGIN_RIGHT_MAXIMIZE = 0

/**
 * Value of margin right when footer is expanded down
 */
const val FOOTER_CONTAINER_MARGIN_RIGHT_MINIMIZE = 32

/**
 * URI of ADR 2017 pdf on UNECE website
 */
const val ADR_PDF_URI = "https://www.unece.org/fr/trans/danger/publi/adr/adr2017/17contentsf.html"

/**
 * Name of local database
 */
const val DATABASE_NAME = "adr_db"

/**
 * URL of the distant reference database
 */
const val DATABASE_SERVER_URL = "http://adr.podam.gendarmerie.fr/adr"

/**
 * URI to get update
 */
const val DATABASE_SERVER_UPDATE_URI = "$DATABASE_SERVER_URL/controler/update.php"

/**
 * URI to get distant reference database version
 */
const val DATABASE_SERVER_VERSION_URI = "$DATABASE_SERVER_URL/controler/version.php"

/**
 * Field to add to update URI, to specify local database version number
 */
const val DATABASE_SERVER_VERSION_FIELD = "?version="

/**
 * Field to add to update URI, to specify local database version date
 */
const val DATABASE_SERVER_TIMESTAMP_FIELD = "&timestamp="

/**
 * Timeout for network request in millisecond
 */
const val VOLLEY_TIMEOUT_MS = 10000

/**
 * Date pattern of dates stored in database
 */
const val DATE_PATTERN = "yyyy-MM-dd HH:mm:ss"

/**
 * Date pattern used for UI display
 */
const val DATE_PATTERN_HUMAN_READABLE = "dd MMMM yyyy à HH:mm:ss"

package fr.gendarmerie.stsisi.adr.ui.fragment.information;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Objects;

import fr.gendarmerie.stsisi.adr.R;
import fr.gendarmerie.stsisi.adr.interfaces.layout.LayoutHeaderChange;
import fr.gendarmerie.stsisi.adr.ui.fragment.information.agreement.AgreementFragment;
import fr.gendarmerie.stsisi.adr.ui.fragment.information.copyright.CopyrightFragment;
import fr.gendarmerie.stsisi.adr.ui.fragment.information.disclaimer.DisclaimerFragment;

import static fr.gendarmerie.stsisi.adr.utilities.ConstantsKt.ADR_PDF_URI;

public class InformationFragment extends Fragment {

    private Activity mParentActivity;
    private LayoutHeaderChange mActivityCallback;
    private View mView;
    private Resources mResources;

    private Button fragment_information_button_adr,
            fragment_information_button_copyright,
            fragment_information_button_legal_disclaimer,
            fragment_information_button_general_terms_of_use;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity) {
            mParentActivity = (Activity) context;

            try {
                mActivityCallback = (LayoutHeaderChange) mParentActivity;
            } catch (ClassCastException e) {
                throw new ClassCastException(mParentActivity.toString()
                        + " must implement LayoutHeaderChange");
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Set fragment attribute and inflate layout
        mView = inflater.inflate(R.layout.fragment_information, container, false);
        mResources = mParentActivity.getResources();

        this.getViews();

        setHeader();

        this.setListeners();

        return mView;
    }

    private void getViews() {
        fragment_information_button_adr = mView.findViewById(R.id.fragment_information_button_adr);
        fragment_information_button_copyright = mView.findViewById(R.id.fragment_information_button_copyright);
        fragment_information_button_legal_disclaimer = mView.findViewById(R.id.fragment_information_button_legal_disclaimer);
        fragment_information_button_general_terms_of_use = mView.findViewById(R.id.fragment_information_button_general_terms_of_use);
    }

    private void setHeader(){
        // Change layout layout_portrait_header properties through MainActivity's interface LayoutHeaderChange
        mActivityCallback.showButtonBack();
        mActivityCallback.changeLogo(R.drawable.ic_help_white);
        mActivityCallback.hideHeaderTitle();
    }

    private void setListeners() {
        fragment_information_button_adr.setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(ADR_PDF_URI));
            startActivity(browserIntent);
        });

        fragment_information_button_copyright.setOnClickListener(v -> Objects.requireNonNull(getFragmentManager())
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.activity_main_fragment_container, new CopyrightFragment())
                .commit());

        fragment_information_button_legal_disclaimer.setOnClickListener(v -> Objects.requireNonNull(getFragmentManager())
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.activity_main_fragment_container, new DisclaimerFragment())
                .commit());

        fragment_information_button_general_terms_of_use.setOnClickListener(v -> Objects.requireNonNull(getFragmentManager())
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.activity_main_fragment_container, new AgreementFragment())
                .commit());
    }
}
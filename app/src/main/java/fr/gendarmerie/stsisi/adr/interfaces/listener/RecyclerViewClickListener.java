package fr.gendarmerie.stsisi.adr.interfaces.listener;

public interface RecyclerViewClickListener {
    void onClick(int position);
}

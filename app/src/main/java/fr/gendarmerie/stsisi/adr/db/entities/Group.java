package fr.gendarmerie.stsisi.adr.db.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

@Entity(tableName = "group",
    indices = {
        @Index("fk_category")
    },
    foreignKeys = {
        @ForeignKey(entity = Category.class,
            parentColumns = "id",
            childColumns = "fk_category")
        }
    )
public class Group extends fr.gendarmerie.stsisi.adr.db.Entity {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    @Ignore
    private static final String TAG = Group.class.getSimpleName();

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private Integer mId=0;

    @NonNull
    @ColumnInfo(name = "fk_category")
    private Integer mFkCategory=0;

    @NonNull
    @ColumnInfo(name = "code")
    private String mCode="";

    @NonNull
    @ColumnInfo(name = "description")
    private String mDescription="";

    @ColumnInfo(name = "comment")
    private String mComment;


    public Group(@NonNull Integer id, @NonNull Integer fkCategory, @NonNull String code, @NonNull String description, String comment) {
        this.mId = id;
        this.mFkCategory = fkCategory;
        this.mCode = code;
        this.mDescription = description;
        this.mComment = comment;
        setEntityAttribute();
    }

    public Group(@NonNull JSONObject json) {
        try {
            this.mId = json.getInt("id");
            this.mFkCategory = json.getInt("fk_category");
            this.mCode = json.getString("code");
            this.mDescription = json.getString("description");
            this.mComment = json.getString("comment");
            setEntityAttribute();
        }
        catch (JSONException exception){
            Log.e(TAG, "Error parsing JSON : "+exception.getCause()+" "+exception.getMessage());
        }
    }

    private void setEntityAttribute(){
        this.mIdentification = Integer.toString(mId);
        this.mTableName = this.getClass().getSimpleName();
        this.mShortDescription = this.mCode+" "+this.mDescription;
    }

    @NonNull
    public Integer getId() {
        return this.mId;
    }

    @NonNull
    public Integer getFkCategory() {
        return mFkCategory;
    }

    @NonNull
    public String getCode() {
        return mCode;
    }

    @NonNull
    public String getDescription() {
        return mDescription;
    }

    public String getComment() {
        return mComment;
    }

}
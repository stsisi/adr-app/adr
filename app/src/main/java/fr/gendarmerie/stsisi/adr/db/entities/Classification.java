package fr.gendarmerie.stsisi.adr.db.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

@Entity(tableName = "classification",
    indices = {
        @Index("fk_category"),
        @Index("fk_hazard"),
        @Index("fk_state"),
        @Index("fk_type")
    },
    foreignKeys = {
        @ForeignKey(entity = Category.class,
            parentColumns = "id",
            childColumns = "fk_category"),
        @ForeignKey(entity = Hazard.class,
            parentColumns = "id",
            childColumns = "fk_hazard"),
        @ForeignKey(entity = State.class,
            parentColumns = "id",
            childColumns = "fk_state"),
        @ForeignKey(entity = Type.class,
            parentColumns = "id",
            childColumns = "fk_type")
    }
)
public class Classification extends fr.gendarmerie.stsisi.adr.db.Entity {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    @Ignore
    private static final String TAG = Category.class.getSimpleName();

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private Integer mId=0;

    @NonNull
    @ColumnInfo(name = "fk_category")
    private Integer mFkCategory=0;

    @NonNull
    @ColumnInfo(name = "fk_hazard")
    private Integer mFkHazard=0;

    @NonNull
    @ColumnInfo(name = "fk_state")
    private Integer mFkState=0;

    @NonNull
    @ColumnInfo(name = "fk_type")
    private Integer mFkType=0;

    public Classification(@NonNull Integer id,
                          @NonNull Integer fkCategory,
                          @NonNull Integer fkHazard,
                          @NonNull Integer fkState,
                          @NonNull Integer fkType) {

        this.mId = id;
        this.mFkCategory = fkCategory;
        this.mFkHazard = fkHazard;
        this.mFkState = fkState;
        this.mFkType = fkType;
        setEntityAttribute();
    }

    public Classification(@NonNull JSONObject json) {
        try {
            this.mId = json.getInt("id");
            this.mFkCategory = json.getInt("fk_category");
            this.mFkHazard = json.getInt("fk_hazard");
            this.mFkState = json.getInt("fk_state");
            this.mFkType = json.getInt("fk_type");
            setEntityAttribute();
        }
        catch (JSONException exception){
            Log.e(TAG, "Error parsing JSON : "+exception.getCause()+" "+exception.getMessage());
        }
    }

    private void setEntityAttribute(){
        this.mIdentification = Integer.toString(mId);
        this.mTableName = this.getClass().getSimpleName();
        this.mShortDescription = "category: "+this.mFkCategory+"; hazard: "+this.mFkHazard+"; state: "+this.mFkState+"; type: "+this.mFkType;
    }

    public Integer getId(){
        return this.mId;
    }

    public Integer getFkCategory(){
        return this.mFkCategory;
    }

    public Integer getFkHazard(){
        return this.mFkHazard;
    }

    public Integer getFkState(){
        return this.mFkState;
    }

    public Integer getFkType(){
        return this.mFkType;
    }

}

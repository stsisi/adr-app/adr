package fr.gendarmerie.stsisi.adr.ui.fragment.unCode;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fr.gendarmerie.stsisi.adr.R;
import fr.gendarmerie.stsisi.adr.interfaces.layout.LayoutHeaderChange;
import fr.gendarmerie.stsisi.adr.interfaces.listener.RecyclerViewClickListener;
import fr.gendarmerie.stsisi.adr.interfaces.search.UnCodeNavigation;
import fr.gendarmerie.stsisi.adr.utilities.Item;
import fr.gendarmerie.stsisi.adr.ui.adapter.ItemListAdapter;
import fr.gendarmerie.stsisi.adr.viewmodel.SearchByCodeFragmentViewModel;

public class SearchByCodeFragment extends Fragment implements UnCodeNavigation, RecyclerViewClickListener {

    private Activity mParentActivity;
    private LayoutHeaderChange mActivityCallback;
    private Context mAppContext;
    private View mView;
    private Resources mResources;
    private ProgressBar fragment_search_by_code_progressBar_wait;
    private EditText fragment_search_by_code_editText_kemler,
            fragment_search_by_code_editText_unNumber;
    private CheckBox fragment_search_by_code_checkbox_X;
    private RecyclerView fragment_search_by_code_recyclerView_search_result;
    private RecyclerView.Adapter mAdapter;
    private ImageView fragment_search_by_code_imageView_search,
            fragment_entity_plate_imageView_next,
            fragment_entity_plate_imageView_previous;

    private SearchByCodeFragmentViewModel mViewModel;
    private List<Item> mResults;
    private int mFirstUnCode = 0,
            mLastUnCode = 0;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mAppContext = context.getApplicationContext();

        if (context instanceof Activity) {
            mParentActivity = (Activity) context;

            try {
                mActivityCallback = (LayoutHeaderChange) mParentActivity;
            } catch (ClassCastException e) {
                throw new ClassCastException(mParentActivity.toString()
                        + " must implement LayoutHeaderChange");
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Set fragment attribute and inflate layout
        mView = inflater.inflate(R.layout.fragment_search_by_code, container, false);

        mResources = mParentActivity.getResources();
        mViewModel = new SearchByCodeFragmentViewModel(mParentActivity.getApplication(), this);

        // Change layout layout_portrait_header properties through MainActivity's interface LayoutHeaderChange
        mActivityCallback.showButtonBack();
        mActivityCallback.hideLogo();
        mActivityCallback.changeTitle("Recherche par code");

        this.getViews();

        this.setViews();

        this.setListeners();

        return mView;
    }

    @Override
    public void onStart() {
        super.onStart();

        // If unNumber EditText is not null at view creation, we automatically launch search
        if(!fragment_search_by_code_editText_unNumber.getText().toString().isEmpty()){
            searchScenario();
        }
    }

    private void getViews() {
        fragment_search_by_code_progressBar_wait = mView.findViewById(R.id.fragment_search_by_code_progressBar_wait);
        fragment_search_by_code_editText_kemler = mView.findViewById(R.id.fragment_entity_plate_textView_kemler);
        fragment_search_by_code_editText_unNumber = mView.findViewById(R.id.fragment_entity_plate_textView_unNumber);
        fragment_search_by_code_checkbox_X = mView.findViewById(R.id.fragment_search_by_code_checkbox_X);
        fragment_search_by_code_recyclerView_search_result = mView.findViewById(R.id.fragment_search_by_code_recyclerView_search_result);
        fragment_search_by_code_imageView_search = mView.findViewById(R.id.fragment_search_by_code_imageView_search);
        fragment_entity_plate_imageView_previous = mView.findViewById(R.id.fragment_entity_plate_imageView_previous);
        fragment_entity_plate_imageView_next = mView.findViewById(R.id.fragment_entity_plate_imageView_next);
    }

    private void setViews() {

        // Set wait progress bar
        fragment_search_by_code_progressBar_wait.setIndeterminate(true);

        // set recyclerView and adapter
        LinearLayoutManager layoutManager = new LinearLayoutManager(mAppContext);
        fragment_search_by_code_recyclerView_search_result.setLayoutManager(layoutManager);

        mResults = new ArrayList<>();

        mAdapter = new ItemListAdapter(mAppContext, mResults, this);
        fragment_search_by_code_recyclerView_search_result.setAdapter(mAdapter);

        //mView.findViewById(R.id.fragment_update_constraintView).requestLayout();

        /*layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);*/
    }

    private void setListeners() {

        // Launch search
        fragment_search_by_code_imageView_search.setOnClickListener(v -> searchScenario());

        // Stop operations
        fragment_search_by_code_progressBar_wait.setOnClickListener(v -> cancelScenario());

        fragment_search_by_code_editText_unNumber.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                searchScenario();
                return true;
            }
            return false;
        });

        fragment_entity_plate_imageView_previous.setOnClickListener(v -> previousScenario());
        fragment_entity_plate_imageView_next.setOnClickListener(v -> nextScenario());
    }

    private void searchScenario(){
        hideButtonSearch();
        showProgressBarWait();
        search();
        hideKeyboard();
    }

    private void cancelScenario(){
        hideProgressBarWait();
        showButtonSearch();
    }

    private void previousScenario(){
        hideButtonSearch();
        showProgressBarWait();
        String unCode = fragment_search_by_code_editText_unNumber.getText().toString();
        if(unCode.isEmpty() || Integer.parseInt(unCode)==mFirstUnCode){
            getLastUnCode();
        }
        else{
            getPreviousUnCode(Integer.parseInt(unCode));
        }
    }

    private void nextScenario(){
        hideButtonSearch();
        showProgressBarWait();
        String unCode = fragment_search_by_code_editText_unNumber.getText().toString();
        if(unCode.isEmpty() || Integer.parseInt(unCode)==mLastUnCode){
            getFirstUnCode();
        }
        else{
            getNextUnCode(Integer.parseInt(unCode));
        }
    }

    private void notifyUnCodeScenario(int un_code){
        fragment_search_by_code_editText_unNumber.setText(String.valueOf(un_code));
        fragment_search_by_code_editText_kemler.setText("");
        searchScenario();
    }

    private void search() {
        String unNumber = fragment_search_by_code_editText_unNumber.getText().toString();
        String kemler = fragment_search_by_code_editText_kemler.getText().toString();

        if (!kemler.isEmpty()) {
            if(fragment_search_by_code_checkbox_X.isChecked())
                kemler = "X"+kemler;
            if (unNumber.isEmpty()) {
                mViewModel.searchMaterialByCode(kemler);
            }
            else {
                mViewModel.searchMaterialByCode(Integer.parseInt(unNumber), kemler);
            }
        }
        else if(!unNumber.isEmpty()) {
            mViewModel.searchMaterialByCode(Integer.parseInt(unNumber));
        }
        else{
            notifySearchEmpty();
        }
    }

    private void getPreviousUnCode(int current){
        mViewModel.getPreviousUnCode(current);
    }

    private void getNextUnCode(int current){
        mViewModel.getNextUnCode(current);
    }

    private void getFirstUnCode(){
        mViewModel.getFirstUnCode();
    }

    private void getLastUnCode(){
        mViewModel.getLastUnCode();
    }

    private void showButtonSearch() {
        fragment_search_by_code_imageView_search.setVisibility(View.VISIBLE);
    }

    private void hideButtonSearch() {
        fragment_search_by_code_imageView_search.setVisibility(View.INVISIBLE);
    }

    private void showProgressBarWait() {
        fragment_search_by_code_progressBar_wait.setVisibility(View.VISIBLE);
    }

    private void hideProgressBarWait() {
        fragment_search_by_code_progressBar_wait.setVisibility(View.INVISIBLE);
    }

    private void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) mParentActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if(inputMethodManager!=null){
            //Find the currently focused view, so we can grab the correct window token from it.
            View view = mParentActivity.getCurrentFocus();
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if(view ==null){
                view = new View(mParentActivity);
            }
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(),0);
        }
    }

    @Override
    public void notifySearchResult(List<Item> results) {
        mParentActivity.runOnUiThread(() -> {
            hideProgressBarWait();
            showButtonSearch();
            mResults.clear();
            mResults.addAll(results);
            mAdapter.notifyDataSetChanged();
        });
    }

    @Override
    public void notifySearchEmpty(){
        mParentActivity.runOnUiThread(() -> {
            hideProgressBarWait();
            showButtonSearch();
            mResults.clear();
            mAdapter.notifyDataSetChanged();
            Toast.makeText(mAppContext, mResources.getString(R.string.no_result), Toast.LENGTH_LONG).show();
        });
    }

    @Override
    public void notifyFirstItemUnCode(int unCode) {
        mFirstUnCode = unCode;
        mParentActivity.runOnUiThread(() -> notifyUnCodeScenario(unCode));
    }

    @Override
    public void notifyLastItemUnCode(int unCode) {
        mLastUnCode = unCode;
        mParentActivity.runOnUiThread(() -> notifyUnCodeScenario(unCode));
    }

    @Override
    public void notifyNextItemUnCode(int unCode) {
        mParentActivity.runOnUiThread(() -> notifyUnCodeScenario(unCode));
    }

    @Override
    public void notifyPreviousItemUnCode(int unCode) {
        mParentActivity.runOnUiThread(() -> notifyUnCodeScenario(unCode));
    }

    @Override
    public void onClick(int position) {

        Item current = mResults.get(position);
        EntityFragment entityFragment = new EntityFragment();
        Bundle args = new Bundle();
        args.putSerializable("searchResult", current);
        entityFragment.setArguments(args);
        FragmentManager fragmentManager = getFragmentManager();
        if(fragmentManager!=null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.addToBackStack("searchByIcon");
            fragmentTransaction.replace(R.id.activity_main_fragment_container, entityFragment);
            fragmentTransaction.commit();
        }
    }
}
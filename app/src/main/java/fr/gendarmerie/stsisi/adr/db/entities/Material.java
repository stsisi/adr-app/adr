package fr.gendarmerie.stsisi.adr.db.entities;

import android.arch.persistence.room.*;
import android.support.annotation.*;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

@Entity(tableName = "material",
    indices = {
        @Index("fk_category"),
        @Index("fk_classification"),
        @Index("fk_group"),
        @Index("fk_kemler"),
        @Index("fk_name"),
        @Index("fk_tunnel")
    },
    foreignKeys = {
        @ForeignKey(entity = Category.class,
            parentColumns = "id",
            childColumns = "fk_category"),
        @ForeignKey(entity = Classification.class,
            parentColumns = "id",
            childColumns = "fk_classification"),
        @ForeignKey(entity = Group.class,
            parentColumns = "id",
            childColumns = "fk_group"),
        @ForeignKey(entity = Kemler.class,
            parentColumns = "id",
            childColumns = "fk_kemler"),
        @ForeignKey(entity = Name.class,
            parentColumns = "id",
            childColumns = "fk_name"),
        @ForeignKey(entity = Tunnel.class,
            parentColumns = "id",
            childColumns = "fk_tunnel")
    })
public class Material extends fr.gendarmerie.stsisi.adr.db.Entity {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    @Ignore
    private static final String TAG = Material.class.getSimpleName();

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private Integer mId=0;

    @NonNull
    @ColumnInfo(name = "fk_category")
    private Integer mFkCategory=0;

    @ColumnInfo(name = "fk_classification")
    private Integer mFkClassification;

    @ColumnInfo(name = "fk_group")
    private Integer mFkGroup;

    @ColumnInfo(name = "fk_kemler")
    private Integer mFkKemler;

    @NonNull
    @ColumnInfo(name = "fk_name")
    private Integer mFkName=0;

    @ColumnInfo(name = "fk_tunnel")
    private Integer mFkTunnel;

    @NonNull
    @ColumnInfo(name = "un_code")
    private Integer mUnCode=0;

    public Material(@NonNull Integer id,
                    @NonNull Integer fkCategory,
                    Integer fkClassification,
                    Integer fkGroup,
                    Integer fkKemler,
                    @NonNull Integer fkName,
                    Integer fkTunnel,
                    @NonNull Integer unCode) {

        this.mId = id;
        this.mFkCategory = fkCategory;
        this.mFkClassification = fkClassification;
        this.mFkGroup = fkGroup;
        this.mFkKemler = fkKemler;
        this.mFkName = fkName;
        this.mFkTunnel = fkTunnel;
        this.mUnCode = unCode;
        setEntityAttribute();
    }

    public Material(@NonNull JSONObject json){
        try {
            this.mId = json.getInt("id");
            this.mFkCategory = json.getInt("fk_category");
            this.mFkClassification = json.isNull("fk_classification")? null: json.optInt("fk_classification");
            this.mFkGroup = json.isNull("fk_group")? null: json.optInt("fk_group");
            this.mFkKemler = json.isNull("fk_kemler")? null: json.optInt("fk_kemler");
            this.mFkName = json.getInt("fk_name");
            this.mFkTunnel = json.isNull("fk_tunnel")? null: json.optInt("fk_tunnel");
            this.mUnCode = json.getInt("un_code");
            setEntityAttribute();
        }
        catch (JSONException exception){
            Log.e(TAG, "Error parsing JSON : "+exception.getCause()+" "+exception.getMessage());
        }
    }

    private void setEntityAttribute(){
        this.mIdentification = Integer.toString(mId);
        this.mTableName = this.getClass().getSimpleName();
        this.mShortDescription = this.mUnCode.toString();

    }

    @NonNull
    public Integer getId() {
        return mId;
    }

    @NonNull
    public Integer getFkCategory() {
        return mFkCategory;
    }

    public Integer getFkClassification() {
        return mFkClassification;
    }

    public Integer getFkGroup() {
        return mFkGroup;
    }

    public Integer getFkKemler() {
        return mFkKemler;
    }

    @NonNull
    public Integer getFkName() {
        return mFkName;
    }

    public Integer getFkTunnel() {
        return mFkTunnel;
    }

    @NonNull
    public Integer getUnCode() {
        return mUnCode;
    }

}
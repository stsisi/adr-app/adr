package fr.gendarmerie.stsisi.adr.interfaces.layout;

/**
 *
 */
public interface FooterExpand {

    /**
     *
     */
    enum State{up, down}

    /**
     *
     * @param state
     */
    void expand(State state);
}

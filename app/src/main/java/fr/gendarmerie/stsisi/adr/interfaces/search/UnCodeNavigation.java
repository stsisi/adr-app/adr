package fr.gendarmerie.stsisi.adr.interfaces.search;

public interface UnCodeNavigation extends SearchResultNotifier {
    void notifyFirstItemUnCode(int unCode);
    void notifyLastItemUnCode(int unCode);
    void notifyNextItemUnCode(int unCode);
    void notifyPreviousItemUnCode(int unCode);
}

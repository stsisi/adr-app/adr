package fr.gendarmerie.stsisi.adr.db.update;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static fr.gendarmerie.stsisi.adr.utilities.ConstantsKt.DATE_PATTERN;
import static fr.gendarmerie.stsisi.adr.utilities.ConstantsKt.DATE_PATTERN_HUMAN_READABLE;

public class DatabaseVersion {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    private static final String TAG = DatabaseVersion.class.getSimpleName();

    private final String mVersionNumber;
    private final Date mVersionDate;

    DatabaseVersion(String versionNumber, String versionDate){
        this.mVersionNumber = versionNumber;
        this.mVersionDate = this.stringToDate(versionDate);
    }

    String getVersionNumber() {
        return mVersionNumber;
    }

    private Date getVersionDateObject() {
        return mVersionDate;
    }

    private Boolean isUptoDate(Date when){
        return !this.mVersionDate.before(when);
    }

    Boolean isUptoDate(DatabaseVersion otherVersion){
        return isUptoDate(otherVersion.getVersionDateObject());
    }

    String getVersionDateString(){
        return dateToString(mVersionDate);
    }

    private String dateToString(Date date) {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
        return dateFormat.format(date);
    }

    private Date stringToDate(String string){
        Date date=null;
        try {
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
            date = dateFormat.parse(string);
        }
        catch(ParseException exception) {
            Log.w(TAG, "stringToDate : "+exception.getMessage());
        }
        return date;
    }

    @NonNull
    @Override
    public String toString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN_HUMAN_READABLE, Locale.FRENCH);
        return "v"+mVersionNumber+" du "+dateFormat.format(mVersionDate);
    }
}

package fr.gendarmerie.stsisi.adr.layout;

import android.content.SharedPreferences;

import fr.gendarmerie.stsisi.adr.utilities.App;

import static fr.gendarmerie.stsisi.adr.utilities.ConstantsKt.APPLICATION_SHARED_PREFERENCES_DISCLAIMER_DIALOG;

public abstract class AgreementDialog {

    private final static SharedPreferences mSharedPreferences = App.getSharedPreferences();

    public static Boolean isAccepted(){
        return mSharedPreferences.getBoolean(APPLICATION_SHARED_PREFERENCES_DISCLAIMER_DIALOG, false);
    }

    public static void accept(Boolean accepted) {
        mSharedPreferences
                .edit()
                .putBoolean(APPLICATION_SHARED_PREFERENCES_DISCLAIMER_DIALOG, accepted)
                .apply();
    }
}

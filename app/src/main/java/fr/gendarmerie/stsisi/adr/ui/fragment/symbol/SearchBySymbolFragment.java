package fr.gendarmerie.stsisi.adr.ui.fragment.symbol;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import fr.gendarmerie.stsisi.adr.R;
import fr.gendarmerie.stsisi.adr.interfaces.layout.LayoutHeaderChange;
import fr.gendarmerie.stsisi.adr.interfaces.listener.RecyclerViewClickListener;
import fr.gendarmerie.stsisi.adr.interfaces.search.SearchResultNotifier;
import fr.gendarmerie.stsisi.adr.ui.fragment.unCode.EntityFragment;
import fr.gendarmerie.stsisi.adr.utilities.Item;
import fr.gendarmerie.stsisi.adr.ui.adapter.ItemListAdapter;
import fr.gendarmerie.stsisi.adr.viewmodel.SearchBySymbolFragmentViewModel;

public class SearchBySymbolFragment extends Fragment implements SearchResultNotifier, RecyclerViewClickListener {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    private static final String TAG = SearchBySymbolFragment.class.getSimpleName();

    private Activity mParentActivity;
    private LayoutHeaderChange mActivityCallback;
    private Context mAppContext;
    private View mView;
    private Resources mResources;
    private String mCurrentSymbolName;

    private ProgressBar fragment_search_by_symbol_progressBar_wait;
    private RecyclerView fragment_search_by_symbol_recyclerView_search_result;
    private RecyclerView.Adapter mAdapter;

    private SearchBySymbolFragmentViewModel mViewModel;
    private List<Item> mResults;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mAppContext = context.getApplicationContext();

        if (context instanceof Activity) {
            mParentActivity = (Activity) context;

            try {
                mActivityCallback = (LayoutHeaderChange) mParentActivity;
            } catch (ClassCastException e) {
                throw new ClassCastException(mParentActivity.toString()
                        + " must implement LayoutHeaderChange");
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Set fragment attribute and inflate layout
        mView = inflater.inflate(R.layout.fragment_search_by_symbol, container, false);

        mResources = mParentActivity.getResources();
        mViewModel = new SearchBySymbolFragmentViewModel(mParentActivity.getApplication(), this);

        Bundle args = getArguments();
        if(args != null){
            mCurrentSymbolName = args.getString("symbolTag", null);
        }

        setHeader();

        this.getViews();

        this.setViews();

        showProgressBarWait();

        search();

        return mView;
    }

    private void setHeader(){
        // Change layout layout_portrait_header properties through MainActivity's interface LayoutHeaderChange
        mActivityCallback.showButtonBack();
        if(mCurrentSymbolName !=null){
            int id = mResources.getIdentifier(mCurrentSymbolName, "drawable", mAppContext.getPackageName());
            if(id!=0){
                //Drawable drawable = mResources.getDrawable(id);
                mActivityCallback.changeLogo(id);
            }
            else{
                Log.w(TAG , "No drawable found with name " + mCurrentSymbolName);
                mActivityCallback.hideLogo();
            }

            //TODO set name
        }
        else{
            mActivityCallback.hideLogo();
            mActivityCallback.changeTitle(mResources.getString(R.string.fragment_search_by_symbol));
        }
    }

    private void getViews() {
        fragment_search_by_symbol_progressBar_wait = mView.findViewById(R.id.fragment_search_by_symbol_progressBar_wait);
        fragment_search_by_symbol_recyclerView_search_result = mView.findViewById(R.id.fragment_search_by_symbol_recyclerView_search_result);
    }

    private void setViews() {

        // Set wait progress bar
        fragment_search_by_symbol_progressBar_wait.setIndeterminate(true);

        // set recyclerView and adapter
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mAppContext);
        fragment_search_by_symbol_recyclerView_search_result.setLayoutManager(mLayoutManager);

        mResults = new ArrayList<>();

        mAdapter = new ItemListAdapter(mAppContext, mResults, this);
        fragment_search_by_symbol_recyclerView_search_result.setAdapter(mAdapter);

        //mView.findViewById(R.id.fragment_update_constraintView).requestLayout();

        /*mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);*/
    }

    private void search() {
        if (!mCurrentSymbolName.isEmpty()) {
            mViewModel.searchMaterialBySymbol(mCurrentSymbolName);
        }
    }

    private void showProgressBarWait() {
        fragment_search_by_symbol_progressBar_wait.setVisibility(View.VISIBLE);
    }

    private void hideProgressBarWait() {
        fragment_search_by_symbol_progressBar_wait.setVisibility(View.INVISIBLE);
    }

    @Override
    public void notifySearchResult(List<Item> results) {
        mParentActivity.runOnUiThread(() -> {
            hideProgressBarWait();
            mResults.clear();
            mResults.addAll(results);
            mAdapter.notifyDataSetChanged();
        });
    }

    @Override
    public void notifySearchEmpty(){
        mParentActivity.runOnUiThread(() -> {
            hideProgressBarWait();
            mResults.clear();
            mAdapter.notifyDataSetChanged();
        });
    }

    @Override
    public void onClick(int position) {
        Item current = mResults.get(position);
        EntityFragment entityFragment = new EntityFragment();
        Bundle args = new Bundle();
        args.putSerializable("searchResult", current);
        entityFragment.setArguments(args);
        FragmentManager fragmentManager = getFragmentManager();
        if(fragmentManager!=null)
            fragmentManager.beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.activity_main_fragment_container, entityFragment)
                    .commit();
    }
}
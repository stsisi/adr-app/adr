package fr.gendarmerie.stsisi.adr.db.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

@Entity(tableName = "state")
public class State extends fr.gendarmerie.stsisi.adr.db.Entity {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    @Ignore
    private static final String TAG = State.class.getSimpleName();

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private Integer mId=0;

    @ColumnInfo(name = "code")
    private String mCode;

    @ColumnInfo(name = "name")
    private String mName;

    @ColumnInfo(name = "comment")
    private String mComment;


    public State(@NonNull Integer id, String code, String name, String comment) {
        this.mId = id;
        this.mCode = code;
        this.mName = name;
        this.mComment = comment;
        setEntityAttribute();
    }

    public State(@NonNull JSONObject json){
        try {
            this.mId = json.getInt("id");
            this.mCode = json.getString("code");
            this.mName = json.getString("name");
            this.mComment = json.getString("comment");
            setEntityAttribute();
        }
        catch (JSONException exception){
            Log.e(TAG, "Error parsing JSON : "+exception.getCause()+" "+exception.getMessage());
        }
    }

    private void setEntityAttribute(){
        this.mIdentification = Integer.toString(mId);
        this.mTableName = this.getClass().getSimpleName();
        this.mShortDescription = this.mCode+" "+this.mName;
    }

    @NonNull
    public Integer getId() {
        return this.mId;
    }

    public String getCode() {
        return mCode;
    }

    public String getName() {
        return mName;
    }

    public String getComment() {
        return mComment;
    }

}
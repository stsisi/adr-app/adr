package fr.gendarmerie.stsisi.adr.ui.fragment.symbol;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.Objects;

import fr.gendarmerie.stsisi.adr.R;
import fr.gendarmerie.stsisi.adr.interfaces.layout.LayoutHeaderChange;

public class SelectSymbolFragment extends Fragment {


    private Activity mParentActivity;
    private LayoutHeaderChange mActivityCallback;
    private View mView;

    private ImageView fragment_select_symbol_imageView_symbol_1,
            fragment_select_symbol_imageView_symbol_1_4,
            fragment_select_symbol_imageView_symbol_1_5,
            fragment_select_symbol_imageView_symbol_1_6,
            fragment_select_symbol_imageView_symbol_2_1,
            fragment_select_symbol_imageView_symbol_2_2,
            fragment_select_symbol_imageView_symbol_2_3,
            fragment_select_symbol_imageView_symbol_3,
            fragment_select_symbol_imageView_symbol_4_1,
            fragment_select_symbol_imageView_symbol_4_2,
            fragment_select_symbol_imageView_symbol_4_3,
            fragment_select_symbol_imageView_symbol_5_1,
            fragment_select_symbol_imageView_symbol_5_2,
            fragment_select_symbol_imageView_symbol_6_1,
            fragment_select_symbol_imageView_symbol_6_2,
            fragment_select_symbol_imageView_symbol_7a,
            fragment_select_symbol_imageView_symbol_7b,
            fragment_select_symbol_imageView_symbol_7c,
            fragment_select_symbol_imageView_symbol_7d,
            fragment_select_symbol_imageView_symbol_7e,
            fragment_select_symbol_imageView_symbol_8,
            fragment_select_symbol_imageView_symbol_9,
            fragment_select_symbol_imageView_symbol_9a,
            fragment_select_symbol_imageView_symbol_env,
            fragment_select_symbol_imageView_symbol_hot;

    private final View.OnClickListener mosaicListener = v -> {
        String symbolTag = v.getTag().toString();
        Fragment fragment = new SymbolFragment();

        Bundle args = new Bundle();
        args.putString("symbolTag", symbolTag);
        fragment.setArguments(args);

        FragmentTransaction fragmentTransaction = Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.activity_main_fragment_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


        if (context instanceof Activity) {
            mParentActivity = (Activity) context;

            try {
                mActivityCallback = (LayoutHeaderChange) mParentActivity;
            } catch (ClassCastException e) {
                throw new ClassCastException(mParentActivity.toString()
                        + " must implement LayoutHeaderChange");
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Set fragment attribute and inflate layout
        mView = inflater.inflate(R.layout.fragment_select_symbol, container, false);

        Resources mResources = mParentActivity.getResources();

        // Change layout layout_portrait_header properties through MainActivity's interface LayoutHeaderChange
        mActivityCallback.showButtonBack();
        mActivityCallback.changeLogo(R.drawable.ic_mosaic_adr);
        mActivityCallback.changeTitle(mResources.getString(R.string.fragment_search_by_symbol));

        this.getViews();

        this.setListeners();

        return mView;
    }

    private void getViews() {
        fragment_select_symbol_imageView_symbol_1 = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_1);
        fragment_select_symbol_imageView_symbol_1_4 = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_1_4);
        fragment_select_symbol_imageView_symbol_1_5 = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_1_5);
        fragment_select_symbol_imageView_symbol_1_6 = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_1_6);
        fragment_select_symbol_imageView_symbol_2_1 = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_2_1);
        fragment_select_symbol_imageView_symbol_2_2 = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_2_2);
        fragment_select_symbol_imageView_symbol_2_3 = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_2_3);
        fragment_select_symbol_imageView_symbol_3 = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_3);
        fragment_select_symbol_imageView_symbol_4_1 = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_4_1);
        fragment_select_symbol_imageView_symbol_4_2 = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_4_2);
        fragment_select_symbol_imageView_symbol_4_3 = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_4_3);
        fragment_select_symbol_imageView_symbol_5_1 = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_5_1);
        fragment_select_symbol_imageView_symbol_5_2 = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_5_2);
        fragment_select_symbol_imageView_symbol_6_1 = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_6_1);
        fragment_select_symbol_imageView_symbol_6_2 = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_6_2);
        fragment_select_symbol_imageView_symbol_7a = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_7a);
        fragment_select_symbol_imageView_symbol_7b = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_7b);
        fragment_select_symbol_imageView_symbol_7c = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_7c);
        fragment_select_symbol_imageView_symbol_7d = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_7d);
        fragment_select_symbol_imageView_symbol_7e = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_7e);
        fragment_select_symbol_imageView_symbol_8 = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_8);
        fragment_select_symbol_imageView_symbol_9 = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_9);
        fragment_select_symbol_imageView_symbol_9a = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_9a);
        fragment_select_symbol_imageView_symbol_env = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_env);
        fragment_select_symbol_imageView_symbol_hot = mView.findViewById(R.id.fragment_select_symbol_imageView_symbol_hot);
    }

    private void setListeners() {
        fragment_select_symbol_imageView_symbol_1.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_1_4.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_1_5.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_1_6.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_2_1.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_2_2.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_2_3.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_3.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_4_1.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_4_2.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_4_3.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_5_1.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_5_2.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_6_1.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_6_2.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_7a.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_7b.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_7c.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_7d.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_7e.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_8.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_9.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_9a.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_env.setOnClickListener(mosaicListener);
        fragment_select_symbol_imageView_symbol_hot.setOnClickListener(mosaicListener);
    }
}
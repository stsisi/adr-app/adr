package fr.gendarmerie.stsisi.adr.ui.fragment.information.copyright;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import fr.gendarmerie.stsisi.adr.R;
import fr.gendarmerie.stsisi.adr.interfaces.layout.LayoutHeaderChange;
import fr.gendarmerie.stsisi.adr.interfaces.parser.ParserListNotifier;
import fr.gendarmerie.stsisi.adr.ui.adapter.CopyrightListAdapter;
import fr.gendarmerie.stsisi.adr.utilities.Copyright;
import fr.gendarmerie.stsisi.adr.utilities.parser.csv.CsvToCopyrightParser;

public class CopyrightFragment extends Fragment implements ParserListNotifier<Copyright> {

    private Activity mParentActivity;
    private LayoutHeaderChange mActivityCallback;
    private Context mAppContext;
    private View mView,
            fragment_copyright_view_loading;

    private CsvToCopyrightParser mParser;
    private InputStream mInputStream;

    private ProgressBar fragment_copyright_progressBar_loading;
    private RecyclerView fragment_copyright_recyclerView_image_copyright;
    private RecyclerView.Adapter mAdapter;

    private final List<Copyright> mResults = new ArrayList<>();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mAppContext = context.getApplicationContext();

        if (context instanceof Activity) {
            mParentActivity = (Activity) context;

            try {
                mActivityCallback = (LayoutHeaderChange) mParentActivity;
            } catch (ClassCastException e) {
                throw new ClassCastException(mParentActivity.toString()
                        + " must implement LayoutHeaderChange");
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_copyright, container, false);

        Resources mResources = mParentActivity.getResources();

        mInputStream = getResources().openRawResource(R.raw.copyright);
        mParser = new CsvToCopyrightParser(this);
        mAdapter = new CopyrightListAdapter(mAppContext, mResults);

        mActivityCallback.showButtonBack();
        mActivityCallback.changeTitle(mResources.getString(R.string.fragment_copyright));
        mActivityCallback.changeLogo(R.drawable.logo_copyright_black);

        this.getViews();

        this.setViews();

        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mResults.isEmpty()&&mInputStream!=null){
            mParser.execute(mInputStream);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mParser.cancel(true);
    }

    private void getViews(){
        fragment_copyright_progressBar_loading = mView.findViewById(R.id.fragment_copyright_progressBar_loading);
        fragment_copyright_recyclerView_image_copyright = mView.findViewById(R.id.fragment_copyright_recyclerView_image_copyright);
        fragment_copyright_view_loading = mView.findViewById(R.id.fragment_copyright_view_loading);
    }

    private void setViews(){

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mAppContext);
        fragment_copyright_recyclerView_image_copyright.setLayoutManager(mLayoutManager);
        fragment_copyright_recyclerView_image_copyright.setAdapter(mAdapter);
        loadingScenario();

    }

    private void loadingScenario(){
        fragment_copyright_progressBar_loading.setIndeterminate(true);
        fragment_copyright_progressBar_loading.setVisibility(View.VISIBLE);
        fragment_copyright_view_loading.setVisibility(View.VISIBLE);
        fragment_copyright_recyclerView_image_copyright.setVisibility(View.INVISIBLE);
    }

    private void dataReadyScenario(){
        fragment_copyright_progressBar_loading.setVisibility(View.GONE);
        fragment_copyright_view_loading.setVisibility(View.GONE);
        fragment_copyright_recyclerView_image_copyright.setVisibility(View.VISIBLE);
    }

    @Override
    public void onParsingListFinish(List<Copyright> results) {
        mParentActivity.runOnUiThread(() -> {
            dataReadyScenario();
            mResults.clear();
            mResults.addAll(results);
            mAdapter.notifyDataSetChanged();
        });
    }

    @Override
    public void onParsingError() {
        mParentActivity.runOnUiThread(this::loadingScenario);
    }
}
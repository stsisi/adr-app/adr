package fr.gendarmerie.stsisi.adr.utilities;

import android.support.annotation.NonNull;

public class Copyright {

    @NonNull
    private String mUri="";
    @NonNull
    private String mSource="";
    private String mAuthor;
    @NonNull
    private String mLicensing="";
    private String mDescription;

    public Copyright(){

    }

    @NonNull
    public String getUri() {
        return mUri;
    }

    public void setUri(@NonNull String uri) {
        this.mUri = uri;
    }

    @NonNull
    public String getSource() {
        return mSource;
    }

    public void setSource(@NonNull String source) {
        this.mSource = source;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public void setAuthor(String author) {
        this.mAuthor = author;
    }

    @NonNull
    public String getLicensing() {
        return mLicensing;
    }

    public void setLicensing(@NonNull String licensing) {
        this.mLicensing = licensing;
    }

    public String getDescription(){
        return mDescription;
    }

    public void setDescription(String description){
        this.mDescription = description;
    }
}



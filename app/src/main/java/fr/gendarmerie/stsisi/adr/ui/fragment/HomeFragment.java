package fr.gendarmerie.stsisi.adr.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.Objects;

import fr.gendarmerie.stsisi.adr.R;
import fr.gendarmerie.stsisi.adr.db.Entity;
import fr.gendarmerie.stsisi.adr.db.update.DatabaseUpdateSingleton;
import fr.gendarmerie.stsisi.adr.interfaces.db.DatabaseUpdateProgressNotifier;
import fr.gendarmerie.stsisi.adr.interfaces.layout.LayoutHeaderChange;
import fr.gendarmerie.stsisi.adr.ui.fragment.symbol.SelectSymbolFragment;
import fr.gendarmerie.stsisi.adr.ui.fragment.unCode.SearchByCodeFragment;
import fr.gendarmerie.stsisi.adr.utilities.App;

public class HomeFragment extends Fragment implements DatabaseUpdateProgressNotifier{

    private FragmentActivity mParentActivity;
    private LayoutHeaderChange mCallback;
    private View mView;
    private DatabaseUpdateSingleton mDatabaseUpdater;
    private LinearLayout fragment_home_linearLayout_byCode,
            fragment_home_linearLayout_bySymbol;
    private View fragment_home_view_blur;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity activity;

        if (context instanceof Activity){
            activity=(Activity) context;

            try {
                mCallback = (LayoutHeaderChange) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString()
                        + " must implement LayoutHeaderChange");
            }
        }


        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_home, container, false);

        mParentActivity = getActivity();
        assert mParentActivity != null;
        Resources mResources = mParentActivity.getResources();
        mDatabaseUpdater = App.getDatabaseUpdateSingleton();

        mCallback.hideButtonBack();
        mCallback.changeLogo(R.drawable.logo_stsisi);
        mCallback.changeTitle(mResources.getString(R.string.header_welcome));

        getViews();

        setViews();

        setListeners();

        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mDatabaseUpdater.addProgressCallback(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mDatabaseUpdater.removeProgressCallback(this);
    }

    private void getViews(){
        fragment_home_linearLayout_byCode = mView.findViewById(R.id.fragment_home_linearLayout_byCode);
        fragment_home_linearLayout_bySymbol = mView.findViewById(R.id.fragment_home_linearLayout_bySymbol);
        fragment_home_view_blur = mView.findViewById(R.id.fragment_home_view_blur);
    }

    private void setViews(){
        if(App.getDatabaseUpdateSingleton().isUpdateRunning()||!App.getDatabaseUpdateSingleton().isLocalDatabaseExist()) {
            fragment_home_linearLayout_byCode.setEnabled(false);
            fragment_home_linearLayout_bySymbol.setEnabled(false);
            fragment_home_view_blur.setVisibility(View.VISIBLE);
        }
    }

    private void setListeners(){

        fragment_home_linearLayout_byCode.setOnClickListener(v -> {
            SearchByCodeFragment fragment = new SearchByCodeFragment();
            FragmentTransaction fragmentTransaction = Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.activity_main_fragment_container, fragment);
            Objects.requireNonNull(getFragmentManager()).popBackStack("home", 0);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        });

        fragment_home_linearLayout_bySymbol.setOnClickListener(v -> {
            SelectSymbolFragment fragment = new SelectSymbolFragment();
            FragmentTransaction fragmentTransaction = Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.activity_main_fragment_container, fragment);
            Objects.requireNonNull(getFragmentManager()).popBackStack("home", 0);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        });
    }

    private void activateButtons(){
        fragment_home_linearLayout_byCode.setEnabled(true);
        fragment_home_linearLayout_bySymbol.setEnabled(true);
        fragment_home_view_blur.setVisibility(View.GONE);
    }

    @Override
    public void notifyProgress(int progress, int max) {

    }

    @Override
    public void notifyProcessedEntity(Entity entity) {

    }

    @Override
    public void notifyUpdateFinished() {
        mParentActivity.runOnUiThread(this::activateButtons);
    }
}
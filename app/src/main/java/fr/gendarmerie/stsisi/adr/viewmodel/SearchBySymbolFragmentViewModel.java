package fr.gendarmerie.stsisi.adr.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import fr.gendarmerie.stsisi.adr.db.entities.Material;
import fr.gendarmerie.stsisi.adr.interfaces.search.SearchResultNotifier;
import fr.gendarmerie.stsisi.adr.repository.LocalRepository;
import fr.gendarmerie.stsisi.adr.utilities.Item;

public class SearchBySymbolFragmentViewModel extends AndroidViewModel {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    private static final String TAG = SearchByCodeFragmentViewModel.class.getSimpleName();

    private final LocalRepository mRepository;

    private List<Material> mMaterials;
    private final List<Item> mResult;

    private final SearchResultNotifier mCallBack;

    public SearchBySymbolFragmentViewModel(Application application, SearchResultNotifier callBack){
        super(application);
        mRepository = new LocalRepository(application);
        mResult = new ArrayList<>();
        mCallBack = callBack;
    }

    public void searchMaterialBySymbol(String url){
        AsyncTask.execute(() -> {
            if(mCallBack != null) {
                mResult.clear();
                mMaterials = mRepository.getMaterialBySymbol(url);
                if(mMaterials != null){
                    for (Material material: mMaterials) {
                        Item current = new Item(material);
                        mResult.add(current);
                    }
                    mCallBack.notifySearchResult(mResult);
                }
                else{
                    mCallBack.notifySearchEmpty();
                }
            }
            else{
                Log.w(TAG, "searchMaterialBySymbol : No call back interface set, so no call back made... ^_^");
            }
        });
    }
}
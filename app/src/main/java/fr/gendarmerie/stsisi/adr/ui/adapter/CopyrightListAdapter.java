package fr.gendarmerie.stsisi.adr.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import fr.gendarmerie.stsisi.adr.R;
import fr.gendarmerie.stsisi.adr.utilities.Copyright;

public class CopyrightListAdapter extends RecyclerView.Adapter<CopyrightListAdapter.CopyrightViewHolder> {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    private static final String TAG = CopyrightListAdapter.class.getSimpleName();

    class CopyrightViewHolder extends RecyclerView.ViewHolder{

        private final ImageView adapter_list_copyright_imageView_image;
        private final TextView adapter_list_copyright_textView_name,
                adapter_list_copyright_textView_author,
                adapter_list_copyright_textView_source,
                adapter_list_copyright_textView_licence;

        private CopyrightViewHolder(View itemView) {
            super(itemView);
            adapter_list_copyright_textView_name = itemView.findViewById(R.id.adapter_list_copyright_textView_name);
            adapter_list_copyright_textView_author = itemView.findViewById(R.id.adapter_list_copyright_textView_author);
            adapter_list_copyright_textView_source = itemView.findViewById(R.id.adapter_list_copyright_textView_source);
            adapter_list_copyright_textView_licence = itemView.findViewById(R.id.adapter_list_copyright_textView_licence);
            adapter_list_copyright_imageView_image = itemView.findViewById(R.id.adapter_list_copyright_imageView_image);
        }
    }

    private final Context mContext;
    private final Resources mResources;
    private final List<Copyright> mEntities;

    public CopyrightListAdapter(@NonNull Context context, @NonNull List<Copyright> entities) {
        mContext = context;
        mEntities = entities;
        mResources = context.getResources();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public @NonNull CopyrightListAdapter.CopyrightViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_list_copyright, parent, false);
        return new CopyrightListAdapter.CopyrightViewHolder(itemView);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull CopyrightListAdapter.CopyrightViewHolder holder, int position) {
        Copyright currentCopyright = mEntities.get(position);

        holder.adapter_list_copyright_textView_name.setText(currentCopyright.getUri());
        holder.adapter_list_copyright_textView_author.setText(currentCopyright.getAuthor()!=null?currentCopyright.getAuthor():"");
        holder.adapter_list_copyright_textView_source.setText(currentCopyright.getSource());
        String licence = currentCopyright.getLicensing() + (currentCopyright.getDescription()!=null?", "+currentCopyright.getDescription():"");
        holder.adapter_list_copyright_textView_licence.setText(licence);

        String uri = currentCopyright.getUri();
        int id = mResources.getIdentifier(uri, "drawable", mContext.getPackageName());
        if (id != 0) {
            holder.adapter_list_copyright_imageView_image.setImageDrawable(mResources.getDrawable(id));
        }
        else {
            Log.w(TAG, "No drawable found with name " + currentCopyright.getUri());
        }

    }

    // getItemCount() is called many times, and when it is first called,
    // mCopyrights has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mEntities != null)
            return mEntities.size();
        else return 0;
    }
}

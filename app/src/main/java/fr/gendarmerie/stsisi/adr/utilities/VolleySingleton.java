package fr.gendarmerie.stsisi.adr.utilities;

import android.app.Application;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import java.util.Objects;

import fr.gendarmerie.stsisi.adr.interfaces.network.VolleyResultNotifier;

import static fr.gendarmerie.stsisi.adr.utilities.ConstantsKt.VOLLEY_TIMEOUT_MS;

public class VolleySingleton {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    private static final String TAG = VolleySingleton.class.getSimpleName();

    private static VolleySingleton mInstance;
    private final RequestQueue mRequestQueue;

    private final VolleyResultNotifier mResultCallback;

    private VolleySingleton(Application application, VolleyResultNotifier resultCallback) {
        mRequestQueue = Volley.newRequestQueue(application.getApplicationContext());
        mResultCallback = resultCallback;
    }

    public static synchronized VolleySingleton getInstance(Application application, VolleyResultNotifier resultCallback) {
        if (mInstance == null) {
            mInstance = new VolleySingleton(application, resultCallback);
        }

        return mInstance;
    }

    public synchronized void sendGetJsonObjectRequest(final String requestType, String url){
        try {

            JsonObjectRequest jsonObj = new JsonObjectRequest(Request.Method.GET, url, null, response -> {
                Log.d(TAG, requestType+" request from "+url+" return : "+response.toString());
                if(mResultCallback != null) {
                    if (Objects.equals(requestType, "GET VERSION")) {
                        mResultCallback.notifyVersionReceived(response);
                    } else {
                        mResultCallback.notifyJsonObjectReceived(response);
                    }
                }
            }, error -> {
                if(mResultCallback != null)
                    mResultCallback.notifyNetworkError(requestType, error);
            });

            jsonObj.setRetryPolicy(new DefaultRetryPolicy(
                    VOLLEY_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            mRequestQueue.add(jsonObj);
            Log.d(TAG, requestType+" request send to "+url);

        }catch(Exception e){
            Log.e(TAG, "GET request error : "+e.getMessage());
        }
    }
}

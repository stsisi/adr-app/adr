package fr.gendarmerie.stsisi.adr.interfaces.parser;

public interface ParserResultNotifier<T> extends ParserNotifier {
    void onParsingFinish(T result);
}

package fr.gendarmerie.stsisi.adr.utilities.parser.txt;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import fr.gendarmerie.stsisi.adr.interfaces.parser.ParserResultNotifier;
import fr.gendarmerie.stsisi.adr.utilities.parser.csv.CsvToCopyrightParser;

public class FileToTextParser extends AsyncTask<InputStream, Integer, Spanned> {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    private static final String TAG = CsvToCopyrightParser.class.getSimpleName();

    private final ParserResultNotifier<Spanned> mCallback;

    public FileToTextParser(@NonNull ParserResultNotifier<Spanned> callback) {
        this.mCallback = callback;
    }

    @Override
    protected Spanned doInBackground(@NonNull InputStream... inputStreams) {

        StringBuilder disclaimer = new StringBuilder();

        for(InputStream inputStream : inputStreams) {

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            try {
                String line;
                while ((line = reader.readLine()) != null) {
                    if(!line.equals("")) {
                        disclaimer.append(line).append("<br />");
                    }
                    else{
                        disclaimer.append("<br />");
                    }
                }
                reader.close();
            }
            catch(IOException exception){
                Log.e(TAG, "Error while reading disclaimer.txt text file");
            }
        }

        return Html.fromHtml(disclaimer.toString());
    }

    @Override
    protected void onPostExecute(Spanned result) {
        mCallback.onParsingFinish(result);
    }
}

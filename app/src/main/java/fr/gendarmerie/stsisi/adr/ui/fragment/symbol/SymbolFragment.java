package fr.gendarmerie.stsisi.adr.ui.fragment.symbol;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Objects;

import fr.gendarmerie.stsisi.adr.R;
import fr.gendarmerie.stsisi.adr.db.Container;
import fr.gendarmerie.stsisi.adr.db.entities.Symbol;
import fr.gendarmerie.stsisi.adr.interfaces.db.DatabaseRequestResultNotifier;
import fr.gendarmerie.stsisi.adr.interfaces.layout.LayoutHeaderChange;
import fr.gendarmerie.stsisi.adr.utilities.Utilities;
import fr.gendarmerie.stsisi.adr.viewmodel.SymbolFragmentViewModel;

public class SymbolFragment  extends Fragment implements DatabaseRequestResultNotifier {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    private static final String TAG = SymbolFragment.class.getSimpleName();

    private Activity mParentActivity;
    private LayoutHeaderChange mActivityCallback;
    private Context mAppContext;
    private Application mApplication;
    private View mView;
    private Resources mResources;
    private Symbol mCurrentSymbol;

    private TextView fragment_symbol_textView_hazard,
            fragment_symbol_textView_guidance,
            fragment_symbol_textView_description;

    private ConstraintLayout fragment_symbol_constraintLayout_hazard,
            fragment_symbol_constraintLayout_guidance,
            fragment_symbol_constraintLayout_description,
            fragment_symbol_constraintLayout_search;

    private Button fragment_symbol_button_search;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mAppContext = context.getApplicationContext();

        if (context instanceof Activity) {
            mParentActivity = (Activity) context;
            mApplication = mParentActivity.getApplication();

            try {
                mActivityCallback = (LayoutHeaderChange) mParentActivity;
            } catch (ClassCastException e) {
                throw new ClassCastException(mParentActivity.toString()
                        + " must implement LayoutHeaderChange");
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Set fragment attribute and inflate layout
        mView = inflater.inflate(R.layout.fragment_symbol, container, false);
        SymbolFragmentViewModel mViewModel = new SymbolFragmentViewModel(mApplication, this);
        mResources = mParentActivity.getResources();

        Bundle args = getArguments();
        if(args != null){
            String currentSymbolName = args.getString("symbolTag", null);
            mViewModel.getSymbolByName(currentSymbolName);
        }

        this.getViews();

        // Set listeners
        this.setListeners();


        // Finally return the inflated view
        return mView;
    }

    private void setHeader(){
        // Change layout layout_portrait_header properties through MainActivity's interface LayoutHeaderChange
        mActivityCallback.showButtonBack();
        if(mCurrentSymbol!=null){
            int id = mResources.getIdentifier(mCurrentSymbol.getUri(), "drawable", mAppContext.getPackageName());
            if(id!=0){
                //Drawable drawable = mResources.getDrawable(id);
                mActivityCallback.changeLogo(id);
            }
            else{
                Log.w(TAG , "No drawable found with name " + mCurrentSymbol.getUri());
                mActivityCallback.hideLogo();
            }
            mActivityCallback.changeTitle(Utilities.toTitleCase(mCurrentSymbol.getName()));
        }
        else{
            mActivityCallback.hideLogo();
            mActivityCallback.hideHeaderTitle();
        }
    }

    private void getViews() {
        fragment_symbol_textView_hazard = mView.findViewById(R.id.fragment_symbol_textView_hazard);
        fragment_symbol_textView_guidance = mView.findViewById(R.id.fragment_symbol_textView_guidance);
        fragment_symbol_textView_description = mView.findViewById(R.id.fragment_symbol_textView_description);
        fragment_symbol_constraintLayout_hazard = mView.findViewById(R.id.fragment_symbol_constraintLayout_hazard);
        fragment_symbol_constraintLayout_guidance = mView.findViewById(R.id.fragment_symbol_constraintLayout_guidance);
        fragment_symbol_constraintLayout_description = mView.findViewById(R.id.fragment_symbol_constraintLayout_description);
        fragment_symbol_constraintLayout_search = mView.findViewById(R.id.fragment_symbol_constraintLayout_search);
        fragment_symbol_button_search = mView.findViewById(R.id.fragment_symbol_button_search);
    }

    private void setViews() {

            if(mCurrentSymbol!=null) {
                String hazard = mCurrentSymbol.getHazard();
                if(!hazard.isEmpty()) {
                    fragment_symbol_textView_hazard.setText(hazard);
                }
                else{
                    hideHazard();
                }

                String guidance = mCurrentSymbol.getGuidance();
                if(!guidance.isEmpty()) {
                    fragment_symbol_textView_guidance.setText(guidance);
                }
                else{
                    hideGuidance();
                }

                String description = mCurrentSymbol.getDescription();
                if(!description.isEmpty()) {
                    fragment_symbol_textView_description.setText(description);
                }
                else{
                    hideDescription();
                    showSearch();
                }
            }
    }

    private void setListeners() {
        fragment_symbol_button_search.setOnClickListener(v -> {
            Fragment fragment = new SearchBySymbolFragment();

            Bundle args = new Bundle();
            args.putString("symbolTag", mCurrentSymbol.getUri());
            fragment.setArguments(args);

            FragmentTransaction fragmentTransaction = Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.activity_main_fragment_container, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        });
    }

    private void hideHazard(){
        fragment_symbol_constraintLayout_hazard.setVisibility(View.GONE);
    }

    private void hideGuidance(){
        fragment_symbol_constraintLayout_guidance.setVisibility(View.GONE);
    }

    private void hideDescription(){
        fragment_symbol_constraintLayout_description.setVisibility(View.GONE);
    }

    private void showSearch(){
        fragment_symbol_constraintLayout_search.setVisibility(View.VISIBLE);
    }

    @Override
    public void notifyRequestResult(Container container) {

        mParentActivity.runOnUiThread(() -> {
            mCurrentSymbol = container.getSymbols().get(0);
            this.setViews();
            setHeader();
        });
    }

    @Override
    public void notifyRequestEmpty() {
        mParentActivity.runOnUiThread(this::setHeader);
    }
}

package fr.gendarmerie.stsisi.adr.interfaces.db;

/**
 * Interface use to notify the state of the database
 *
 * A database can be in one of these three states :
 *
 * - UpToDate : The database is up to date and don't need to be updated
 * - Unknow : The reference database cannot be contacted, the actual database has an unknown state
 * - Update : The database is not up to date and need to be updated
 * @author remy.persuanne
 * @version 1.0
 */
public interface DatabaseUpdateStateNotifier {
    /**
     * Notify database is up to date and don't need to be updated
     */
    void notifyUpToDate();

    /**
     * Notify database state is unknown
     */
    void notifyUnknown();

    /**
     * Notify database is not up to date and need to be updated
     */
    void notifyUpdate();
}

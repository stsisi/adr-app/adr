package fr.gendarmerie.stsisi.adr.interfaces.parser;

public interface ParserNotifier {
    void onParsingError();
}

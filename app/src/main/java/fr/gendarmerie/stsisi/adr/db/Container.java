package fr.gendarmerie.stsisi.adr.db;

import java.util.List;

import fr.gendarmerie.stsisi.adr.db.entities.Symbol;

/**
 *
 */
public class Container {
    private List<Symbol> mSymbols;

    public Container() {

    }

    public List<Symbol> getSymbols() {
        return mSymbols;
    }

    public void setSymbols(List<Symbol> symbols) {
        this.mSymbols = symbols;
    }

}

package fr.gendarmerie.stsisi.adr.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import fr.gendarmerie.stsisi.adr.db.entities.Category;
import fr.gendarmerie.stsisi.adr.db.entities.Classification;
import fr.gendarmerie.stsisi.adr.db.entities.Country;
import fr.gendarmerie.stsisi.adr.db.entities.Group;
import fr.gendarmerie.stsisi.adr.db.entities.HasSymbol;
import fr.gendarmerie.stsisi.adr.db.entities.Hazard;
import fr.gendarmerie.stsisi.adr.db.entities.Kemler;
import fr.gendarmerie.stsisi.adr.db.entities.Material;
import fr.gendarmerie.stsisi.adr.db.entities.Name;
import fr.gendarmerie.stsisi.adr.db.entities.State;
import fr.gendarmerie.stsisi.adr.db.entities.Symbol;
import fr.gendarmerie.stsisi.adr.db.entities.Tunnel;
import fr.gendarmerie.stsisi.adr.db.entities.Type;

/**
 * Data access object used to manipulate database
 *
 * @author remy.persuanne
 * @version 1.0
 */
@android.arch.persistence.room.Dao
public interface Dao {

    /**
     * Insert a Category entity in database, in case of conflict new data replace the old one
     * @param entity the entity to insert in database
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCategory(Category entity);

    /**
     * Insert a Classification entity in database, in case of conflict new data replace the old one
     * @param entity the entity to insert in database
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertClassification(Classification entity);

    /**
     * Insert a Country entity in database, in case of conflict new data replace the old one
     * @param entity the entity to insert in database
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCountry(Country entity);

    /**
     * Insert a Group entity in database, in case of conflict new data replace the old one
     * @param entity the entity to insert in database
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertGroup(Group entity);

    /**
     * Insert a HasSymbol entity in database, in case of conflict new data replace the old one
     * @param entity the entity to insert in database
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertHasSymbol(HasSymbol entity);

    /**
     * Insert a Hazard entity in database, in case of conflict new data replace the old one
     * @param entity the entity to insert in database
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertHazard(Hazard entity);

    /**
     * Insert a Kemler entity in database, in case of conflict new data replace the old one
     * @param entity the entity to insert in database
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertKemler(Kemler entity);

    /**
     * Insert a Material entity in database, in case of conflict new data replace the old one
     * @param entity the entity to insert in database
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMaterial(Material entity);

    /**
     * Insert a Name entity in database, in case of conflict new data replace the old one
     * @param entity the entity to insert in database
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertName(Name entity);

    /**
     * Insert a State entity in database, in case of conflict new data replace the old one
     * @param entity the entity to insert in database
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertState(State entity);

    /**
     * Insert a Symbol entity in database, in case of conflict new data replace the old one
     * @param entity the entity to insert in database
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSymbol(Symbol entity);

    /**
     * Insert a Tunnel entity in database, in case of conflict new data replace the old one
     * @param entity the entity to insert in database
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTunnel(Tunnel entity);

    /**
     * Insert a Type entity in database, in case of conflict new data replace the old one
     * @param entity the entity to insert in database
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertType(Type entity);

    /**
     * Delete all data from table category
     */
    @Query("DELETE FROM `category`")
    void deleteAllCategory();

    /**
     * Delete all data from table classification
     */
    @Query("DELETE FROM `classification`")
    void deleteAllClassification();

    /**
     * Delete all data from table country
     */
    @Query("DELETE FROM `country`")
    void deleteAllCountry();

    /**
     * Delete all data from table group
     */
    @Query("DELETE FROM `group`")
    void deleteAllGroup();

    /**
     * Delete all data from table hasSymbol
     */
    @Query("DELETE FROM `hasSymbol`")
    void deleteAllHasSymbol();

    /**
     * Delete all data from table hazard
     */
    @Query("DELETE FROM `hazard`")
    void deleteAllHazard();

    /**
     * Delete all data from table kemler
     */
    @Query("DELETE FROM `kemler`")
    void deleteAllKemler();

    /**
     * Delete all data from table material
     */
    @Query("DELETE FROM `material`")
    void deleteAllMaterial();

    /**
     * Delete all data from table name
     */
    @Query("DELETE FROM `name`")
    void deleteAllName();

    /**
     * Delete all data from table state
     */
    @Query("DELETE FROM `state`")
    void deleteAllState();

    /**
     * Delete all data from table symbol
     */
    @Query("DELETE FROM `symbol`")
    void deleteAllSymbol();

    /**
     * Delete all data from table tunnel
     */
    @Query("DELETE FROM `tunnel`")
    void deleteAllTunnel();

    /**
     * Delete all data from table type
     */
    @Query("DELETE FROM `type`")
    void deleteAllType();

    /**
     * Returns a List of all Country stored in database
     *
     * @return List of all Country stored in database
     */
    @Query("SELECT * FROM `country` ORDER BY `name` ASC")
    LiveData<List<Country>> getAllCountry();

    /**
     * Returns a List of Symbol that have an URI corresponding to the one passed in parameter
     *
     * @param uri the requested URI
     * @return List of Symbol with the specified uri
     */
    @Query("SELECT `symbol`.* FROM `symbol`" +
            "WHERE `symbol`.`uri` LIKE :uri ")
    List<Symbol> getSymbolByUri(String uri);

    /**
     * Returns a List of Material that have a Symbol with an URI corresponding
     * to the one passed in parameter
     *
     * @param uri the requested URI
     * @return List of Material that have a Symbol with the specified uri
     */
    @Query("SELECT `material`.* FROM `material` " +
            "INNER JOIN `hasSymbol` ON `hasSymbol`.`fk_material` = `material`.`id`" +
            "INNER JOIN `symbol` ON `symbol`.`id` = `hasSymbol`.`fk_symbol`" +
            "WHERE `symbol`.`uri` LIKE :uri ")
    List<Material> getMaterialBySymbol(String uri);

    /**
     * Returns a List of Material that have an un_code corresponding to the one passed in parameter
     *
     * @param code the requested un_code
     * @return List of Material with the specified un_code
     */
    @Query("SELECT `material`.* FROM `material` " +
            "WHERE `material`.`un_code` = :code ")
    List<Material> getMaterialByCode(int code);

    /**
     *
     * @param code
     * @param kemler
     * @return
     */
    @Query("SELECT `material`.* FROM `material` " +
            "INNER JOIN `kemler` ON `material`.`fk_kemler` = `kemler`.id " +
            "WHERE `material`.`un_code` = :code AND `kemler`.`code` LIKE :kemler")
    List<Material> getMaterialByCode(int code, String kemler);

    /**
     *
     * @param kemler
     * @return
     */
    @Query("SELECT `material`.* FROM `material` " +
            "INNER JOIN `kemler` ON `material`.`fk_kemler` = `kemler`.id " +
            "WHERE `kemler`.`code` LIKE :kemler")
    List<Material> getMaterialByCode(String kemler);

    /**
     *
     * @return
     */
    @Query("SELECT MIN(`material`.`un_code`) FROM `material`")
    int getFirstUnCode();

    /**
     *
     * @return
     */
    @Query("SELECT MAX(`material`.`un_code`) FROM `material`")
    int getLastUnCode();

    /**
     *
     * @param current
     * @return
     */
    @Query("SELECT MIN(`material`.`un_code`) FROM `material`" +
            "WHERE `material`.`un_code` > :current")
    int getNextUnCode(int current);

    /**
     *
     * @param current
     * @return
     */
    @Query("SELECT MAX(`material`.`un_code`) FROM `material`" +
            "WHERE `material`.`un_code` < :current")
    int getPreviousUnCode(int current);

    /**
     *
     * @param materialId
     * @return
     */
    @Query("SELECT `category`.* FROM `category` " +
            "INNER JOIN `material` ON `material`.`fk_category` = `category`.`id` " +
            "WHERE `material`.`id` = :materialId ")
    Category getCategoryByMaterial(int materialId);

    /**
     *
     * @param materialId
     * @return
     */
    @Query("SELECT `classification`.* FROM `classification` " +
            "INNER JOIN `material` ON `material`.`fk_classification` = `classification`.`id` " +
            "WHERE `material`.`id` = :materialId ")
    Classification getClassificationByMaterial(int materialId);

    /**
     *
     * @param materialId
     * @return
     */
    @Query("SELECT `group`.* FROM `group` " +
            "INNER JOIN `material` ON `material`.`fk_group` = `group`.`id` " +
            "WHERE `material`.`id` = :materialId ")
    Group getGroupByMaterial(int materialId);

    /**
     *
     * @param classificationId
     * @return
     */
    @Query("SELECT `hazard`.* FROM `hazard` " +
            "INNER JOIN `classification` ON `classification`.`fk_hazard` = `hazard`.`id` " +
            "WHERE `classification`.`id` = :classificationId ")
    Hazard getHazardByClassification(int classificationId);

    /**
     *
     * @param materialId
     * @return
     */
    @Query("SELECT `kemler`.* FROM `kemler` " +
            "INNER JOIN `material` ON `material`.`fk_kemler` = `kemler`.`id` " +
            "WHERE `material`.`id` = :materialId ")
    Kemler getKemlerByMaterial(int materialId);

    /**
     *
     * @param materialId
     * @return
     */
    @Query("SELECT `name`.* FROM `name` " +
            "INNER JOIN `material` ON `material`.`fk_name` = `name`.`id` " +
            "WHERE `material`.`id` = :materialId ")
    Name getNameByMaterial(int materialId);

    /**
     *
     * @param materialId
     * @return
     */
    @Query("SELECT `symbol`.* FROM `symbol` " +
            "INNER JOIN `hasSymbol` ON `hasSymbol`.`fk_symbol` = `symbol`.`id` " +
            "WHERE `hasSymbol`.`fk_material` = :materialId ")
    List<Symbol> getAllSymbolByMaterial(int materialId);

    /**
     *
     * @param classificationId
     * @return
     */
    @Query("SELECT `state`.* FROM `state` " +
            "INNER JOIN `classification` ON `classification`.`fk_state` = `state`.`id` " +
            "WHERE `classification`.`id` = :classificationId ")
    State getStateByClassification(int classificationId);

    /**
     *
     * @param materialId
     * @return
     */
    @Query("SELECT `tunnel`.* FROM `tunnel` " +
            "INNER JOIN `material` ON `material`.`fk_tunnel` = `tunnel`.`id` " +
            "WHERE `material`.`id` = :materialId ")
    Tunnel getTunnelByMaterial(int materialId);

    /**
     *
     * @param classificationId
     * @return
     */
    @Query("SELECT `type`.* FROM `type` " +
            "INNER JOIN `classification` ON `classification`.`fk_type` = `type`.`id` " +
            "WHERE `classification`.`id` = :classificationId ")
    Type getTypeByClassification(int classificationId);
}

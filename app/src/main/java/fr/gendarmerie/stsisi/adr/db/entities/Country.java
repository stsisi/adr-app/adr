package fr.gendarmerie.stsisi.adr.db.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

@Entity(tableName = "country")
public class Country  extends fr.gendarmerie.stsisi.adr.db.Entity {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    @Ignore
    private static final String TAG = Country.class.getSimpleName();

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private Integer mId=0;

    @NonNull
    @ColumnInfo(name = "name")
    private String mName="";

    @ColumnInfo(name = "flag_uri")
    private String mFlagUri="";

    public Country(@NonNull Integer id, @NonNull String name, String flagUri){
        this.mId = id;
        this.mName = name;
        this.mFlagUri = flagUri;
        setEntityAttribute();
    }

    public Country(@NonNull JSONObject json){
        try {
            this.mId = json.getInt("id");
            this.mName = json.getString("name");
            this.mFlagUri = json.getString("flag_uri");
            setEntityAttribute();
        }
        catch (JSONException exception){
            Log.e(TAG, "Error parsing JSON : "+exception.getCause()+" "+exception.getMessage());
        }
    }

    public String getFlagUri() {
        return mFlagUri;
    }

    private void setEntityAttribute(){
        this.mIdentification = Integer.toString(mId);
        this.mTableName = this.getClass().getSimpleName();
        this.mShortDescription = this.mName+" "+this.getFlagUri();
    }

    @NonNull
    public Integer getId() {
        return mId;
    }

    @NonNull
    public String getName() {
        return mName;
    }

}
package fr.gendarmerie.stsisi.adr.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import fr.gendarmerie.stsisi.adr.db.entities.Material;
import fr.gendarmerie.stsisi.adr.interfaces.search.UnCodeNavigation;
import fr.gendarmerie.stsisi.adr.interfaces.search.SearchResultNotifier;
import fr.gendarmerie.stsisi.adr.repository.LocalRepository;
import fr.gendarmerie.stsisi.adr.utilities.App;
import fr.gendarmerie.stsisi.adr.utilities.Item;

public class SearchByCodeFragmentViewModel extends AndroidViewModel {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    private static final String TAG = SearchByCodeFragmentViewModel.class.getSimpleName();

    private final LocalRepository mRepository;

    private List<Material> mMaterials;
    private final List<Item> mResult;

    private final SearchResultNotifier mCallBackResult;
    private UnCodeNavigation mCallBackNavigation;

    public SearchByCodeFragmentViewModel(Application application, SearchResultNotifier callBack){
        super(application);
        mRepository = App.getLocalRepository();
        mResult = new ArrayList<>();
        mCallBackResult = callBack;
    }

    public SearchByCodeFragmentViewModel(Application application, UnCodeNavigation callBack){
        super(application);
        mRepository = App.getLocalRepository();
        mResult = new ArrayList<>();
        mCallBackResult = callBack;
        mCallBackNavigation = callBack;
    }

    public void searchMaterialByCode(int code){
        AsyncTask.execute(() -> {
            if(mCallBackResult != null) {
                mResult.clear();
                mMaterials = mRepository.getMaterialByUnCode(code);
                if(!mMaterials.isEmpty()){
                    for (Material material: mMaterials) {
                        Item current = new Item(material);
                        mResult.add(current);
                    }
                    mCallBackResult.notifySearchResult(mResult);
                }
                else{
                    mCallBackResult.notifySearchEmpty();
                }
            }
            else{
                Log.w(TAG, "searchMaterialByUnCode : No call back interface set, so no call back made... ^_^");
            }
        });
    }

    public void searchMaterialByCode(int code, String kemler){
        AsyncTask.execute(() -> {
            if(mCallBackResult != null) {
                mResult.clear();
                mMaterials = mRepository.getMaterialByUnCode(code, kemler);
                if(!mMaterials.isEmpty()){
                    for (Material material: mMaterials) {
                        Item current = new Item(material);
                        mResult.add(current);
                    }
                    mCallBackResult.notifySearchResult(mResult);
                }
                else{
                    mCallBackResult.notifySearchEmpty();
                }
            }
            else{
                Log.w(TAG, "searchMaterialByUnCode : No call back interface set, so no call back made... ^_^");
            }
        });
    }

    public void searchMaterialByCode(String kemler){
        AsyncTask.execute(() -> {
            if(mCallBackResult != null) {
                mResult.clear();
                mMaterials = mRepository.getMaterialByKemlerCode(kemler);
                if(!mMaterials.isEmpty()){
                    for (Material material: mMaterials) {
                        Item current = new Item(material);
                        mResult.add(current);
                    }
                    mCallBackResult.notifySearchResult(mResult);
                }
                else{
                    mCallBackResult.notifySearchEmpty();
                }
            }
            else{
                Log.w(TAG, "searchMaterialByUnCode : No call back interface set, so no call back made... ^_^");
            }
        });
    }

    public void getFirstUnCode(){
        AsyncTask.execute(() -> {
            if(mCallBackNavigation != null) {
                mCallBackNavigation.notifyFirstItemUnCode(mRepository.getFirstUnCode());
            }
            else{
                Log.w(TAG, "getFirstUnCode : No call back interface set, so no call back made... ^_^");
            }
        });
    }

    public void getLastUnCode(){
        AsyncTask.execute(() -> {
            if(mCallBackNavigation != null) {
                mCallBackNavigation.notifyLastItemUnCode(mRepository.getLastUnCode());
            }
            else{
                Log.w(TAG, "getLastUnCode : No call back interface set, so no call back made... ^_^");
            }
        });
    }

    public void getNextUnCode(int current){
        AsyncTask.execute(() -> {
            if(mCallBackNavigation != null) {
                mCallBackNavigation.notifyNextItemUnCode(mRepository.getNextUnCode(current));
            }
            else{
                Log.w(TAG, "getNextUnCode : No call back interface set, so no call back made... ^_^");
            }
        });
    }

    public void getPreviousUnCode(int current){
        AsyncTask.execute(() -> {
            if(mCallBackNavigation != null) {
                mCallBackNavigation.notifyPreviousItemUnCode(mRepository.getPreviousUnCode(current));
            }
            else{
                Log.w(TAG, "getPreviousUnCode : No call back interface set, so no call back made... ^_^");
            }
        });
    }
}
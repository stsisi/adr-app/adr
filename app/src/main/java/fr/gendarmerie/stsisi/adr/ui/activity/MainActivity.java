package fr.gendarmerie.stsisi.adr.ui.activity;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import fr.gendarmerie.stsisi.adr.R;
import fr.gendarmerie.stsisi.adr.db.Entity;
import fr.gendarmerie.stsisi.adr.interfaces.db.DatabaseUpdateProgressNotifier;
import fr.gendarmerie.stsisi.adr.interfaces.layout.FooterExpand;
import fr.gendarmerie.stsisi.adr.interfaces.layout.LayoutFooterChange;
import fr.gendarmerie.stsisi.adr.interfaces.layout.LayoutHeaderChange;
import fr.gendarmerie.stsisi.adr.ui.fragment.country.CountryFragment;
import fr.gendarmerie.stsisi.adr.ui.fragment.update.FooterUpdateFragment;
import fr.gendarmerie.stsisi.adr.ui.fragment.HomeFragment;
import fr.gendarmerie.stsisi.adr.ui.fragment.information.InformationFragment;
import fr.gendarmerie.stsisi.adr.ui.fragment.unCode.SearchByCodeFragment;
import fr.gendarmerie.stsisi.adr.ui.fragment.symbol.SelectSymbolFragment;
import fr.gendarmerie.stsisi.adr.ui.fragment.update.UpdateFragment;
import fr.gendarmerie.stsisi.adr.utilities.App;

import static fr.gendarmerie.stsisi.adr.utilities.ConstantsKt.FOOTER_CONTAINER_MARGIN_RIGHT_MAXIMIZE;
import static fr.gendarmerie.stsisi.adr.utilities.ConstantsKt.FOOTER_CONTAINER_MARGIN_RIGHT_MINIMIZE;
import static fr.gendarmerie.stsisi.adr.utilities.ConstantsKt.HEADER_DRAWABLE_PREFIX;
import static fr.gendarmerie.stsisi.adr.utilities.Utilities.convertDpToPixel;

/**
 * MainActivity is the only activity in ADR project, it is used as a container
 * for all fragments of the project.
 * It uses a layout called activity_main, composed of a header, a navigation
 * drawer,a container for fragments and a footer. Footer is also a fragments
 * container, it has the particularity to be expandable.
 *
 * MainActivity have to manage navigation drawer events, fragments changes
 * in the main container and back stack.
 *
 * Header and footer modifications are made through Interfaces called
 * LayoutHeaderChange and LayoutFooterChange. Footer expansion uses Interface
 * FooterExpand.
 *
 * Footer is only showed when update is running, to know if an update is going
 * on we use method isUpdateRunning in onResume method, after a click event on
 * an element of navigation drawer or when back button is pressed.
 *
 * @author remy.persuanne
 * @version 1.1
 */

public class MainActivity extends AppCompatActivity implements LayoutHeaderChange, LayoutFooterChange, DatabaseUpdateProgressNotifier {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    private static final String TAG = MainActivity.class.getSimpleName();

    private final List<MenuItem> mMenuItemsToDisabled = new ArrayList<>();

    private Context mAppContext;
    private Resources mResources;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private FooterExpand mFooterCallback;
    private Fragment mFooterFragment;

    private FooterExpand.State mFooterExpandState = FooterExpand.State.up;

    private boolean mDrawerIsOpening = false;

    private ImageView layout_header_imageView_menu,
            layout_header_imageView_back,
            layout_header_imageView_logo,
            layout_footer_imageView_expand,
            layout_footer_imageView_close;

    private View include_logo;
    private View layout_footer;

    private TextView layout_header_textView_title;
    private ConstraintLayout drawer_header_imageView_background;
    private FrameLayout layout_footer_fragment_container;

    private final List<Integer> mDrawableIdentifiers = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAppContext = getApplicationContext();
        mResources = mAppContext.getResources();

        this.getViews();

        this.setViews();

        this.setListeners();

        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.activity_main_fragment_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            HomeFragment homeFragment = new HomeFragment();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            homeFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.activity_main_fragment_container, homeFragment)
                    .addToBackStack("home")
                    .commit();
        }

        LocalDatabaseInitialisation();

        showStartUpDialog();
    }

    @Override
    public void onResume(){
        super.onResume();
        App.getDatabaseUpdateSingleton().addProgressCallback(this);
        isUpdateRunning();
    }

    @Override
    public void onPause() {
        super.onPause();
        App.getDatabaseUpdateSingleton().removeProgressCallback(this);
    }

    /**
     * Get all views from layout
     */
    private void getViews(){
        include_logo = findViewById(R.id.include_logo);

        layout_header_imageView_menu = findViewById(R.id.layout_header_imageView_menu);
        layout_header_imageView_back = findViewById(R.id.layout_header_imageView_back);
        layout_header_imageView_logo = findViewById(R.id.layout_header_imageView_logo);
        layout_header_textView_title = findViewById(R.id.include_title);

        layout_footer = findViewById(R.id.layout_footer_include);
        layout_footer_fragment_container = findViewById(R.id.layout_footer_fragment_container);
        layout_footer_imageView_expand = findViewById(R.id.layout_footer_imageView_expand);
        layout_footer_imageView_close = findViewById(R.id.layout_footer_imageView_close);

        mDrawerLayout = findViewById(R.id.activity_main_drawer_layout);
        mNavigationView = findViewById(R.id.main_activity_nav_view);
        View headerView = mNavigationView.getHeaderView(0);
        drawer_header_imageView_background = headerView.findViewById(R.id.drawer_header_constraintLayout);

        //Drawer items to disable when update running
        mMenuItemsToDisabled.clear();
        mMenuItemsToDisabled.add(mNavigationView.getMenu().findItem(R.id.drawer_view_item_search_by_code));
        mMenuItemsToDisabled.add(mNavigationView.getMenu().findItem(R.id.drawer_view_item_search_by_symbol));
        mMenuItemsToDisabled.add(mNavigationView.getMenu().findItem(R.id.drawer_view_item_list_countries));
    }

    /**
     * Set all views from layout
     */
    private void setViews(){
        setDrawerDrawableIdentifier();

        mNavigationView.setItemIconTintList(null);
    }

    /**
     * Set listeners on layout's views
     */
    private void setListeners(){

        // Menu button open drawer
        layout_header_imageView_menu.setOnClickListener(v -> {
            mDrawerLayout.openDrawer(Gravity.START);
        });

        //Manage Drawer events to change header's background
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                null, 0, 0) {

            public void onDrawerClosed (View drawerView){
                super.onDrawerClosed(drawerView);
                mDrawerIsOpening =false;
            }

            public void onDrawerStateChanged (int newState) {
                super.onDrawerStateChanged(newState);
                if(!mDrawerIsOpening) {
                    changeDrawerBackground();
                    mDrawerIsOpening =true;
                }
            }
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);

        // Back button call previous fragment
        layout_header_imageView_back.setOnClickListener(v -> onBackPressed());

        // Footer buttons
        layout_footer_imageView_close.setOnClickListener(v -> hideFooter());
        layout_footer_imageView_expand.setOnClickListener(v -> expandFooter());


        mNavigationView.setNavigationItemSelectedListener(menuItem -> {

            FragmentManager fragmentManager = getSupportFragmentManager();

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            Fragment fragment = new HomeFragment();

            switch(menuItem.getItemId()){
                default:
                case R.id.drawer_view_item_home:
                    fragmentManager.popBackStack("home", 0);
                    fragmentTransaction.addToBackStack("home");
                    break;
                case R.id.drawer_view_item_search_by_code:
                    fragment = new SearchByCodeFragment();
                    fragmentManager.popBackStack("home", 0);
                    fragmentTransaction.addToBackStack(null);
                    break;
                case R.id.drawer_view_item_search_by_symbol:
                    fragment = new SelectSymbolFragment();
                    fragmentManager.popBackStack("home", 0);
                    fragmentTransaction.addToBackStack(null);
                    break;
                case R.id.drawer_view_item_list_countries:
                    fragment = new CountryFragment();
                    fragmentManager.popBackStack("home", 0);
                    fragmentTransaction.addToBackStack(null);
                    break;
                case R.id.drawer_view_item_update:
                    fragment = new UpdateFragment();
                    fragmentManager.popBackStack("home", 0);
                    fragmentTransaction.addToBackStack(null);
                    break;
                case R.id.drawer_view_item_info:
                    fragment = new InformationFragment();
                    fragmentManager.popBackStack("home", 0);
                    fragmentTransaction.addToBackStack(null);
                    break;
            }
            // close drawer when item is tapped
            mDrawerLayout.closeDrawers();

            fragmentTransaction.replace(R.id.activity_main_fragment_container, fragment);
            fragmentTransaction.commit();
            isUpdateRunning();

            return true;
        });
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        int entry = fragmentManager.getBackStackEntryCount();

        if ( entry > 1) {
            fragmentManager.popBackStack();
//            mNavigationView.getCheckedItem().setChecked(false);
        }
        else{
            finish();
        }

        isUpdateRunning();
    }

    private void isUpdateRunning(){
        if(App.getDatabaseUpdateSingleton().isUpdateRunning()) {
            showFooter();
            disableDrawerButtons();
            mFooterFragment = new FooterUpdateFragment();
            mFooterCallback = (FooterExpand) mFooterFragment;
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.layout_footer_fragment_container, mFooterFragment)
                    .commit();
        }
        else{
            hideFooter();

        }
    }


    private void LocalDatabaseInitialisation(){
        if(!App.getDatabaseUpdateSingleton().isLocalDatabaseExist()) {
            App.getDatabaseUpdateSingleton().update();
        }
    }

    private void showStartUpDialog(){
        //CGU
       /* if(!AgreementDialog.isAccepted()) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            Fragment prev = fragmentManager.findFragmentByTag("disclaimer");
            if (prev != null) {
                fragmentTransaction.remove(prev);
            }

            DialogFragment dialogFragment = new AgreementDialogFragment();
            dialogFragment.setCancelable(false);
            dialogFragment.show(fragmentTransaction, "startup");
        }*/
    }

    private void activateDrawerButtons(){
        for(MenuItem menuItem : mMenuItemsToDisabled) {
            menuItem.setEnabled(true);
            Drawable icon = menuItem.getIcon();
            if(icon != null) {
                icon.mutate().setColorFilter(null);
                menuItem.setIcon(icon);
            }
        }
    }

    public void disableDrawerButtons(){
        for(MenuItem menuItem : mMenuItemsToDisabled) {
            menuItem.setEnabled(false);
            Drawable icon = menuItem.getIcon();
            if(icon != null) {
                icon.mutate().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                menuItem.setIcon(icon);
            }
        }
    }

    /**
     * Set the global List of Integer mDrawableIdentifiers
     *
     * For each drawable corresponding to the pattern
     * HEADER_DRAWABLE_PREFIX+number
     * get the id of the drawable and store it in mDrawableIdentifiers
     */
    private void setDrawerDrawableIdentifier(){
        int id, index = 1;
        do {
            String name = HEADER_DRAWABLE_PREFIX+index;
            id = mResources.getIdentifier(name, "drawable", mAppContext.getPackageName());
            if(id!=0) mDrawableIdentifiers.add(id);
            index++;
        } while(id > 0);
    }

    /**
     * Get a random id from the global List mdrawableIdentifier
     *
     * @return a drawable id randomly selected
     */
    private int getRandomIdentifier(){
        int min=0, max = mDrawableIdentifiers.size()-1;
        Random r = new Random();
        return mDrawableIdentifiers.get(r.nextInt((max - min) + 1) + min);
    }

    /**
     * Change the drawer image background with a drawable
     * chosen randomly from a List of drawable id
     */
    private void changeDrawerBackground(){
        int id = getRandomIdentifier();
        Drawable drawable = mResources.getDrawable(id);
        drawer_header_imageView_background.setBackground(drawable);
    }

    /**
     * Show footer in the main layout
     */
    private void showFooter() {
        if (layout_footer != null) {
            layout_footer.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Hide footer in the main layout
     */
    private void hideFooter() {
        if(layout_footer !=null) {
            layout_footer.setVisibility(View.GONE);
        }
    }

    /**
     * Expand the footer in the main layout
     * depending on the previous state up or down stored in mFooterExpandState,
     * then this state is send to callback fragment using Interface FooterExpand
     */
    private void expandFooter() {
        if(mFooterExpandState==FooterExpand.State.up) {
            mFooterExpandState=FooterExpand.State.down;
            setFooterContainerLayoutMarginEnd(FOOTER_CONTAINER_MARGIN_RIGHT_MINIMIZE);
        }
        else{
            mFooterExpandState=FooterExpand.State.up;
            setFooterContainerLayoutMarginEnd(FOOTER_CONTAINER_MARGIN_RIGHT_MAXIMIZE);
        }
        mFooterCallback.expand(mFooterExpandState);
        toggleFooterButtonExpand();
    }

    /**
     * Change the layout margin end of the footer container depending on its expanded state
     *
     * @param marginInDp the new margin to apply
     */
    private void setFooterContainerLayoutMarginEnd(int marginInDp){
        if(layout_footer_fragment_container != null) {
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
            params.setMarginEnd(convertDpToPixel(marginInDp));
            layout_footer_fragment_container.setLayoutParams(params);
        }
    }

    /**
     * Show footer close button
     */
    private void showFooterButtonClose() {
        if(layout_footer_imageView_close!=null) {
            layout_footer_imageView_close.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Hide footer close button
     */
    private void hideFooterButtonClose() {
        if(layout_footer_imageView_close!=null) {
            layout_footer_imageView_close.setVisibility(View.GONE);
        }
    }

    /**
     * Show footer expand button
     */
    private void showFooterButtonExpand() {
        if(layout_footer_imageView_expand !=null) {
            layout_footer_imageView_expand.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Toggle footer expand button drawable between state up and down drawables
     */
    private void toggleFooterButtonExpand() {
        if(layout_footer_imageView_expand !=null) {
            int id=0;
            if(mFooterExpandState == FooterExpand.State.up){
                id = R.drawable.ic_down_white;
            }
            else{
                id = R.drawable.ic_up_white;
            }
            if(id!=0) {
                layout_footer_imageView_expand.setImageResource(id);
            }
            else{
                Log.w(TAG,"ToggleFooterButtonResize : Drawable not find");
            }
        }
    }

    /**
     * Hide footer expand button
     */
    private void hideFooterButtonExpand() {
        if(layout_footer_imageView_expand !=null) {
            layout_footer_imageView_expand.setVisibility(View.GONE);
        }
    }

    // Interfaces methods

    //LayoutHeaderChange interface

    /**
     * Header's logo is set visible and image is changed with the drawable
     * passed in parameter.
     *
     * @param resId the id of the new logo
     */
    @Override
    public void changeLogo(int resId) {
        this.showLogo();
        if(layout_header_imageView_logo!=null) {
            layout_header_imageView_logo.setImageResource(resId);
        }
    }

    private void showLogo() {
        if(include_logo!=null) {
            include_logo.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideLogo() {
        if(include_logo!=null) {
            include_logo.setVisibility(View.GONE);
        }
    }

    @Override
    public void changeTitle(String text) {
        this.showHeaderTitle();
        if(layout_header_textView_title !=null) {
            layout_header_textView_title.setText(text);
        }
    }

    private void showHeaderTitle() {
        if(layout_header_textView_title !=null) {
            layout_header_textView_title.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideHeaderTitle() {
        if(layout_header_textView_title !=null) {
            layout_header_textView_title.setVisibility(View.GONE);
        }
    }

    @Override
    public void showButtonBack() {
        if(layout_header_imageView_back!=null) {
            layout_header_imageView_back.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideButtonBack() {
        if(layout_header_imageView_back!=null) {
            layout_header_imageView_back.setVisibility(View.GONE);
        }
    }

    //LayoutFooterchange interface's methods

    /**
     * Close the footer by setting its visibility to GONE, it also remove
     * callback object and fragment from footer container
     */
    @Override
    public void closeFooter() {
        layout_footer.setVisibility(View.GONE);
        mFooterCallback = null;
        if(mFooterFragment!=null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .remove(mFooterFragment)
                    .commit();
            mFooterFragment = null;
        }
    }

    /**
     * Show close button at top right of footer and automatically hide expand
     * button.
     */
    @Override
    public void showButtonClose() {
        showFooterButtonClose();
        hideFooterButtonExpand();
    }

    /**
     * Show expand button at top right of footer and automatically hide close
     * button.
     */
    @Override
    public void showButtonExpand() {
        showFooterButtonExpand();
        hideFooterButtonClose();
    }

    @Override
    public void notifyProgress(int progress, int max) {

    }

    @Override
    public void notifyProcessedEntity(Entity entity) {

    }

    /**
     * When update is finished activate drawer menu items
     */
    @Override
    public void notifyUpdateFinished() {
        if(App.getDatabaseUpdateSingleton().isLocalDatabaseExist())
            this.runOnUiThread(this::activateDrawerButtons);
    }
}

package fr.gendarmerie.stsisi.adr.db;

import android.arch.persistence.room.Ignore;

import java.io.Serializable;

/**
 * Ab
 */
public abstract class Entity implements Serializable {
    @Ignore
    protected String mTableName;
    @Ignore
    protected String mIdentification;
    @Ignore
    protected String mShortDescription;

    public String getTableName() {
        return mTableName;
    }

    public String getIdentification() {
        return mIdentification;
    }

    public String getShortDescription() {
        return mShortDescription;
    }
}

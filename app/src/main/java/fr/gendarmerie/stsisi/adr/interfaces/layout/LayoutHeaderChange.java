package fr.gendarmerie.stsisi.adr.interfaces.layout;

import android.graphics.drawable.Drawable;

/**
 * Interface uses by activities to manage their header
 *
 * An activity implementing this interface should have at least
 * - A logo (visible by default)
 * - A title (visible by default)
 * - A back button
 */
public interface LayoutHeaderChange {

    /**
     * Change the layout header logo drawable
     *
     * @param resId the id of the new logo
     */
    void changeLogo(int resId);

    /**
     * Hide header logo
     */
    void hideLogo();

    /**
     * Change header title text
     *
     * @param text the new title
     */
    void changeTitle(String text);

    /**
     * Hide header title
     */
    void hideHeaderTitle();

    /**
     * Show header back button
     */
    void showButtonBack();

    /**
     * Hide header back button
     */
    void hideButtonBack();

}
package fr.gendarmerie.stsisi.adr.utilities;

import android.content.res.Resources;
import android.util.DisplayMetrics;

public class Utilities {

    public static int convertDpToPixel(int dp){
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        int px = dp * (metrics.densityDpi / 160);
        return Math.round(px);
    }

    public static String firstCharToUpperCase(String string){
        if (string == null) {
            return null;
        }

        return string.substring(0,1).toUpperCase() + string.substring(1);
    }

    public static String toTitleCase(String string) {
        if (string == null) {
            return null;
        }

        boolean space = true;
        StringBuilder builder = new StringBuilder(string);
        final int len = builder.length();

        char previous=' ';

        for (int i = 0; i < len; ++i) {
            char c = builder.charAt(i);
            if (space) {
                if (!Character.isWhitespace(c)) {
                    // Convert to title case and switch out of whitespace mode.
                    builder.setCharAt(i, Character.toTitleCase(c));
                    previous = c;
                    space = false;
                }
            } else if (Character.isWhitespace(c)) {
                space = true;
            } else if (c=='’') {
                space = true;
                builder.setCharAt(i-1, Character.toLowerCase(previous));
            } else if (c == 'I' && c == previous) {
                builder.setCharAt(i, Character.toUpperCase(c));
            } else {
                builder.setCharAt(i, Character.toLowerCase(c));
            }
        }

        return builder.toString();
    }

    public static String formatUnCode(String un_code) {
        if (un_code == null) {
            return null;
        }

        if(un_code.length()<4){
            un_code = "0"+un_code;
            un_code = formatUnCode(un_code);
        }

        return un_code;

    }
}

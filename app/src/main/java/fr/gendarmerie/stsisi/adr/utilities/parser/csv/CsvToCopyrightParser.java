package fr.gendarmerie.stsisi.adr.utilities.parser.csv;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import fr.gendarmerie.stsisi.adr.interfaces.parser.ParserListNotifier;
import fr.gendarmerie.stsisi.adr.utilities.Copyright;

public class CsvToCopyrightParser extends AsyncTask<InputStream, Integer, List<Copyright>> {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    private static final String TAG = CsvToCopyrightParser.class.getSimpleName();

    private final ParserListNotifier<Copyright> mCallback;
    private List<Copyright> mCopyrights;

    public CsvToCopyrightParser(@NonNull ParserListNotifier<Copyright> callback){
        this.mCallback = callback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        mCopyrights = new ArrayList<>();
    }

    @Override
    protected List<Copyright> doInBackground(@NonNull InputStream... inputStreams) {
        for(InputStream inputStream : inputStreams) {
            // Reads text from character-input stream, buffering characters for efficient reading
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(inputStream, Charset.forName("UTF-8"))
            );

            // Initialization
            String line = "";

            // Initialization
            try {
                // If buffer is not empty
                while ((line = reader.readLine()) != null) {
                    Log.d(TAG, "Line: " + line);
                    // use comma as separator columns of CSV
                    String[] tokens = line.split(";");
                    // Read the data
                    Copyright sample = new Copyright();

                    // Setters
                    if(tokens.length==5) {
                        sample.setUri(tokens[0].replace("\"", ""));
                        sample.setSource(tokens[1].replace("\"", ""));
                        sample.setAuthor(tokens[2].replace("\"", ""));
                        sample.setLicensing(tokens[3].replace("\"", ""));
                        sample.setDescription(tokens[4].replace("\"", ""));
                    }
                    else{
                        Log.w(TAG, "Line "+line+" not parsed, format error");
                    }

                    // Adding object to a class
                    mCopyrights.add(sample);

                    // Log the object
                    Log.d(TAG, "Copyright created: " + sample);
                }

            } catch (IOException exception) {
                // Logs error with priority level
                Log.e(TAG, "Error reading csv file on line" + line, exception);

                // Prints throwable details
                exception.printStackTrace();
                mCallback.onParsingError();
                break;
            }
        }

        return mCopyrights;
    }

    @Override
    protected void onPostExecute(List<Copyright> copyrights) {
        mCallback.onParsingListFinish(copyrights);
    }
}


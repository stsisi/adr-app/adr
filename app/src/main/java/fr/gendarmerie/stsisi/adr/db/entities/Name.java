package fr.gendarmerie.stsisi.adr.db.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

@Entity(tableName = "name")
public class Name extends fr.gendarmerie.stsisi.adr.db.Entity {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    @Ignore
    private static final String TAG = Name.class.getSimpleName();

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private Integer mId=0;

    @NonNull
    @ColumnInfo(name = "fr")
    private String mNameFr="";

    @ColumnInfo(name = "en")
    private String mNameEn="";

    public Name(@NonNull Integer id, @NonNull String nameFr, String nameEn) {
        this.mId = id;
        this.mNameEn = nameEn;
        this.mNameFr = nameFr;
        setEntityAttribute();
    }

    public Name(@NonNull JSONObject json){
        try {
            this.mId = json.getInt("id");
            this.mNameEn = json.getString("en");
            this.mNameFr = json.getString("fr");
            setEntityAttribute();
        }
        catch (JSONException exception){
            Log.e(TAG, "Error parsing JSON : "+exception.getCause()+" "+exception.getMessage());
        }
    }

    private void setEntityAttribute(){
        this.mIdentification = Integer.toString(mId);
        this.mTableName = this.getClass().getSimpleName();
        this.mShortDescription = this.mNameFr;
    }

    @NonNull
    public Integer getId() {
        return mId;
    }

    @NonNull
    public String getNameFr() {
        return mNameFr;
    }

    public String getNameEn() {
        return mNameEn;
    }

}

package fr.gendarmerie.stsisi.adr.db.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

@Entity(tableName = "hasSymbol",
    primaryKeys = { "fk_material", "fk_symbol" },
    indices = {
            @Index("fk_material"),
            @Index("fk_symbol")
    },
    foreignKeys = {
        @ForeignKey(entity = Material.class,
            parentColumns = "id",
            childColumns = "fk_material"),
        @ForeignKey(entity = Symbol.class,
            parentColumns = "id",
            childColumns = "fk_symbol")
    })
public class HasSymbol extends fr.gendarmerie.stsisi.adr.db.Entity {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    @Ignore
    private static final String TAG = HasSymbol.class.getSimpleName();

    @NonNull
    @ColumnInfo(name = "fk_material")
    private Integer mFkMaterial=0;

    @NonNull
    @ColumnInfo(name = "fk_symbol")
    private Integer mFkSymbol=0;

    public HasSymbol(@NonNull Integer fkMaterial, @NonNull Integer fkSymbol) {
        this.mFkMaterial = fkMaterial;
        this.mFkSymbol = fkSymbol;
        setEntityAttribute();
    }

    public HasSymbol(@NonNull JSONObject json){
        try {
            this.mFkMaterial = json.getInt("fk_material");
            this.mFkSymbol = json.getInt("fk_symbol");
            setEntityAttribute();
        }
        catch (JSONException exception){
            Log.e(TAG, "Error parsing JSON : "+exception.getCause()+" "+exception.getMessage());
        }
    }

    private void setEntityAttribute(){
        this.mIdentification = mFkMaterial+":"+mFkSymbol;
        this.mTableName = this.getClass().getSimpleName();
        this.mShortDescription = "fk material: "+this.mFkMaterial+" fk symbol: "+this.mFkSymbol;
    }

    @NonNull
    public Integer getFkMaterial() {
        return mFkMaterial;
    }

    @NonNull
    public Integer getFkSymbol() {
        return mFkSymbol;
    }

}

package fr.gendarmerie.stsisi.adr.ui.fragment.information.disclaimer;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.InputStream;

import fr.gendarmerie.stsisi.adr.R;
import fr.gendarmerie.stsisi.adr.interfaces.layout.LayoutHeaderChange;
import fr.gendarmerie.stsisi.adr.interfaces.parser.ParserResultNotifier;
import fr.gendarmerie.stsisi.adr.utilities.parser.txt.FileToTextParser;

public class DisclaimerFragment extends Fragment implements ParserResultNotifier<Spanned> {

    private Activity mParentActivity;
    private LayoutHeaderChange mActivityCallback;
    private View mView;
    private Spanned mResult;

    private FileToTextParser mParser;
    private InputStream mInputStream;

    private TextView fragment_disclaimer_textView_content;
    private ProgressBar fragment_disclaimer_progressBar_loading;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity) {
            mParentActivity = (Activity) context;

            try {
                mActivityCallback = (LayoutHeaderChange) mParentActivity;
            } catch (ClassCastException e) {
                throw new ClassCastException(mParentActivity.toString()
                        + " must implement LayoutHeaderChange");
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_disclaimer, container, false);

        Resources mResources = mParentActivity.getResources();

        mInputStream = getResources().openRawResource(R.raw.disclaimer);
        mParser = new FileToTextParser(this);

        mActivityCallback.showButtonBack();
        mActivityCallback.changeTitle(mResources.getString(R.string.fragment_general_terms_of_use));
        mActivityCallback.hideLogo();
        //mActivityCallback.changeLogo(mResources.getDrawable(R.drawable.logo_copyright_black));

        this.getViews();

        this.setViews();

        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mResult==null&&mInputStream!=null){
            mParser.execute(mInputStream);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mParser.cancel(true);
    }

    private void getViews(){
        fragment_disclaimer_textView_content = mView.findViewById(R.id.fragment_disclaimer_textView_content);
        fragment_disclaimer_progressBar_loading = mView.findViewById(R.id.fragment_disclaimer_progressBar_loading);
    }

    private void setViews(){
        loadingScenario();
    }

    private void loadingScenario(){
        fragment_disclaimer_progressBar_loading.setIndeterminate(true);
        fragment_disclaimer_progressBar_loading.setVisibility(View.VISIBLE);
    }

    private void dataReadyScenario(){
        fragment_disclaimer_progressBar_loading.setVisibility(View.GONE);
    }

    @Override
    public void onParsingFinish(Spanned result) {
        mResult = result;
        fragment_disclaimer_textView_content.setText(mResult);
        dataReadyScenario();
    }

    @Override
    public void onParsingError() {
        mParentActivity.runOnUiThread(this::loadingScenario);
    }
}

package fr.gendarmerie.stsisi.adr.ui.fragment.country;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fr.gendarmerie.stsisi.adr.R;
import fr.gendarmerie.stsisi.adr.interfaces.layout.LayoutHeaderChange;
import fr.gendarmerie.stsisi.adr.ui.adapter.CountryListAdapter;
import fr.gendarmerie.stsisi.adr.viewmodel.CountryFragmentViewModel;

public class CountryFragment extends Fragment {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    private static final String TAG = CountryFragment.class.getSimpleName();

    private LayoutHeaderChange mCallback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity activity;

        if (context instanceof Activity){
            activity=(Activity) context;

            try {
                mCallback = (LayoutHeaderChange) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString()
                        + " must implement LayoutHeaderChange");
            }
        }
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_country, container, false);
        FragmentActivity mParentActivity = getActivity();

        assert mParentActivity != null;
        Resources mResources = mParentActivity.getResources();

        mCallback.showButtonBack();
        mCallback.changeTitle(mResources.getString(R.string.fragment_country_member));
        mCallback.changeLogo(R.drawable.ic_earth_white);

        RecyclerView fragment_country_recyclerView_countries = mView.findViewById(R.id.fragment_country_recyclerView_countries);

        final CountryListAdapter adapter = new CountryListAdapter(mParentActivity);
        fragment_country_recyclerView_countries.setAdapter(adapter);
        fragment_country_recyclerView_countries.setLayoutManager(new LinearLayoutManager(mParentActivity));

        CountryFragmentViewModel mCountriesListActivityViewModel = ViewModelProviders.of(this).get(CountryFragmentViewModel.class);

        mCountriesListActivityViewModel.getAllCountries().observe(this, countries -> {
            // Update the cached copy of the countries in the adapter.
            adapter.setCountries(countries);
            if(countries != null)
                Log.d(TAG, "Number of countries : "+countries.size());
        });

        return mView;
    }


}

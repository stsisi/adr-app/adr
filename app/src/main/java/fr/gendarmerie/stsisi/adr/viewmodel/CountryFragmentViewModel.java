package fr.gendarmerie.stsisi.adr.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import fr.gendarmerie.stsisi.adr.db.entities.Country;
import fr.gendarmerie.stsisi.adr.repository.LocalRepository;

public class CountryFragmentViewModel extends AndroidViewModel {

    private final LiveData<List<Country>> mAllCountries;

    public CountryFragmentViewModel(Application application){
        super(application);
        LocalRepository mRepository = new LocalRepository(application);
        mAllCountries = mRepository.getAllCountry();
    }

    public LiveData<List<Country>> getAllCountries(){
        return mAllCountries;
    }

}

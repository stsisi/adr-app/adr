package fr.gendarmerie.stsisi.adr.interfaces.db;

import fr.gendarmerie.stsisi.adr.db.Entity;

/**
 * Interface use to notify update progress of a database
 *
 * @author remy.persuanne
 * @version 1.0
 */
public interface DatabaseUpdateProgressNotifier {

    /**
     * Caller notifies progress update to implementors
     *
     * Progress generally represents the number of elements processed or a percentage
     * Max represents the end limit of progress update, generally the total of elements to process or 100 in case of percentage
     *
     * @param progress progress of the update
     * @param max end limit of the progress update
     */
    void notifyProgress(int progress, int max);

    /**
     * Caller notifies implementors that a new entity has been processed
     *
     * @param entity Entity that was processed
     */
    void notifyProcessedEntity(Entity entity);

    /**
     * Caller notifies implementors that the update is finished
     */
    void notifyUpdateFinished();
}

package fr.gendarmerie.stsisi.adr.ui.fragment.update;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import fr.gendarmerie.stsisi.adr.R;
import fr.gendarmerie.stsisi.adr.db.Entity;
import fr.gendarmerie.stsisi.adr.db.update.DatabaseUpdateSingleton;
import fr.gendarmerie.stsisi.adr.interfaces.db.DatabaseUpdateProgressNotifier;
import fr.gendarmerie.stsisi.adr.interfaces.layout.FooterExpand;
import fr.gendarmerie.stsisi.adr.interfaces.layout.LayoutFooterChange;
import fr.gendarmerie.stsisi.adr.utilities.App;

public class FooterUpdateFragment extends Fragment  implements DatabaseUpdateProgressNotifier, FooterExpand {

    private Activity mParentActivity;
    private LayoutFooterChange mActivityCallback;
    private View mView;
    private DatabaseUpdateSingleton mDatabaseUpdater;

    private TextView fragment_footer_update_textView_title,
            fragment_footer_update_textView_task;
    private ProgressBar fragment_footer_update_progressBar_update;
    private View fragment_footer_update_view_separation;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            mParentActivity = (Activity) context;

            try {
                mActivityCallback = (LayoutFooterChange) mParentActivity;
            } catch (ClassCastException e) {
                throw new ClassCastException(mParentActivity.toString()
                        + "Parent activity must implement LayoutFooterChange");
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Set fragment attribute and inflate layout
        mView = inflater.inflate(R.layout.fragment_footer_update, container, false);

        mDatabaseUpdater = App.getDatabaseUpdateSingleton();

        mActivityCallback.showButtonExpand();

        this.getViews();

        this.setViews();

        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mDatabaseUpdater.addProgressCallback(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mDatabaseUpdater.removeProgressCallback(this);
    }

    private void getViews() {
        fragment_footer_update_textView_title = mView.findViewById(R.id.fragment_footer_update_textView_title);
        fragment_footer_update_progressBar_update = mView.findViewById(R.id.fragment_footer_update_progressBar_update);
        fragment_footer_update_textView_task = mView.findViewById(R.id.fragment_footer_update_textView_task);
        fragment_footer_update_view_separation = mView.findViewById(R.id.fragment_footer_update_view_separation);
    }

    private void setViews() {
        // Set update progress bar
        fragment_footer_update_progressBar_update.setIndeterminate(true);
    }

    private void showTitle(){
        fragment_footer_update_textView_title.setVisibility(View.VISIBLE);
    }

    private void hideTitle(){
        fragment_footer_update_textView_title.setVisibility(View.GONE);
    }

    private void showTask(){
        fragment_footer_update_textView_task.setVisibility(View.VISIBLE);
    }

    private void hideTask(){
        fragment_footer_update_textView_task.setVisibility(View.GONE);
    }

    private void showSeparationView(){
        fragment_footer_update_view_separation.setVisibility(View.VISIBLE);
    }

    private void hideSeparationView(){
        fragment_footer_update_view_separation.setVisibility(View.GONE);
    }

    private void expandUpScenario(){
        showTitle();
        showSeparationView();
        showTask();
    }

    private void expandDownScenario(){
        hideTitle();
        hideSeparationView();
        hideTask();
    }

    @Override
    public void notifyProgress(int progress, int max) {
        mParentActivity.runOnUiThread(() -> {
            fragment_footer_update_textView_task.setText(R.string.fragment_footer_update_running);
            fragment_footer_update_progressBar_update.setProgress(progress);
            fragment_footer_update_progressBar_update.setMax(max);
            fragment_footer_update_progressBar_update.setIndeterminate(false);
        });
    }

    @Override
    public void notifyProcessedEntity(Entity entity) {

    }

    @Override
    public void notifyUpdateFinished() {
        mParentActivity.runOnUiThread(() -> {
            mActivityCallback.showButtonClose();
            fragment_footer_update_textView_task.setText(R.string.fragment_footer_update_done);
        });
    }

    @Override
    public void expand(State state) {
        if(state == State.up){
            expandUpScenario();
        }
        else{
            expandDownScenario();
        }
    }
}
package fr.gendarmerie.stsisi.adr.interfaces.layout;

/**
 *
 */
public interface LayoutFooterChange {

    /**
     *
     */
    void closeFooter();

    /**
     *
     */
    void showButtonClose();

    /**
     *
     */
    void showButtonExpand();
}
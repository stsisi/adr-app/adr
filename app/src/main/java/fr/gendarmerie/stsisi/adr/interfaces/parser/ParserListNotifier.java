package fr.gendarmerie.stsisi.adr.interfaces.parser;

import java.util.List;

public interface ParserListNotifier<T> extends ParserNotifier {
    void onParsingListFinish(List<T> results);
}


package fr.gendarmerie.stsisi.adr.interfaces.db;

import fr.gendarmerie.stsisi.adr.db.update.DatabaseVersion;

/**
 * Interface use to notify database version when a version request is sent
 *
 * @author remy.persuanne
 * @version 1.0
 */
public interface DatabaseVersionCheckNotifier {

    /**
     * Caller notifies database version to implementors
     * @param version the current version of database
     */
    void notifyVersion(DatabaseVersion version);
}

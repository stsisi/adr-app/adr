package fr.gendarmerie.stsisi.adr.utilities;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatDelegate;
import android.util.DisplayMetrics;

import java.util.Properties;

import fr.gendarmerie.stsisi.adr.db.update.DatabaseUpdateSingleton;
import fr.gendarmerie.stsisi.adr.repository.LocalRepository;

import static fr.gendarmerie.stsisi.adr.utilities.ConstantsKt.APPLICATION_SHARED_PREFERENCES;

public class App extends Application {
    private static DatabaseUpdateSingleton mDatabaseUpdateSingleton;
    private static SharedPreferences mSharedPreferences;
    private static LocalRepository mRepository;
    private static float mHeight, mWidth;

    // This flag should be set to true to enable VectorDrawable support for API < 21
    static
    {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Context mContext = this;
        mDatabaseUpdateSingleton = DatabaseUpdateSingleton.getInstance(this);
        mSharedPreferences = getSharedPreferences(APPLICATION_SHARED_PREFERENCES,Context.MODE_PRIVATE);
        mRepository = new LocalRepository(this);
        setProxy();

        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        mHeight = displayMetrics.heightPixels;
        mWidth = displayMetrics.widthPixels;
    }

    public static DatabaseUpdateSingleton getDatabaseUpdateSingleton(){
        return mDatabaseUpdateSingleton;
    }

    public static SharedPreferences getSharedPreferences(){
        return mSharedPreferences;
    }

    public static LocalRepository getLocalRepository() {
        return mRepository;
    }

    public static int getHeight(){
        return (int) mHeight;
    }

    public static int getWidth(){
        return (int) mWidth;
    }

    private void setProxy() {
        Properties systemSettings = System.getProperties();
        systemSettings.setProperty("http.proxyHost", "127.0.0.1");
        systemSettings.setProperty("http.proxyPort", "80");
        systemSettings.setProperty("https.proxyHost", "127.0.0.1");
        systemSettings.setProperty("https.proxyPort", "80");
    }
}

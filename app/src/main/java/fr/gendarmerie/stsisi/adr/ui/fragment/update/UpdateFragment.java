package fr.gendarmerie.stsisi.adr.ui.fragment.update;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.gendarmerie.stsisi.adr.R;
import fr.gendarmerie.stsisi.adr.db.Entity;
import fr.gendarmerie.stsisi.adr.db.update.DatabaseUpdateSingleton;
import fr.gendarmerie.stsisi.adr.db.update.DatabaseVersion;
import fr.gendarmerie.stsisi.adr.interfaces.db.DatabaseUpdateProgressNotifier;
import fr.gendarmerie.stsisi.adr.interfaces.db.DatabaseUpdateStateNotifier;
import fr.gendarmerie.stsisi.adr.interfaces.db.DatabaseVersionCheckNotifier;
import fr.gendarmerie.stsisi.adr.interfaces.layout.LayoutFooterChange;
import fr.gendarmerie.stsisi.adr.interfaces.layout.LayoutHeaderChange;
import fr.gendarmerie.stsisi.adr.ui.activity.MainActivity;
import fr.gendarmerie.stsisi.adr.ui.adapter.UpdateProgressEntityListAdapter;
import fr.gendarmerie.stsisi.adr.utilities.App;

public class UpdateFragment extends Fragment implements DatabaseUpdateStateNotifier, DatabaseUpdateProgressNotifier, DatabaseVersionCheckNotifier {

    private Activity mParentActivity;
    private LayoutHeaderChange mHeaderCallback;
    private LayoutFooterChange mFooterCallback;
    private View mView;
    private Resources mResources;
    private Context mAppContext;
    private DatabaseUpdateSingleton mDatabaseUpdater;
    private TextView fragment_update_textView_version,
            fragment_update_textView_state,
            fragment_update_textView_progress;
    private ImageView fragment_update_imageView_update,
            fragment_update_imageView_check_update,
            fragment_update_imageView_delete;
    private ProgressBar fragment_update_progressBar_update,
            fragment_update_progressBar_wait;
    private RecyclerView fragment_update_recyclerView;
    private UpdateProgressEntityListAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private ConstraintLayout fragment_update_constraintView_update;
    private List<Entity> mAllEntities;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mAppContext = context.getApplicationContext();

        if (context instanceof Activity){
            mParentActivity = (Activity) context;

            try {
                mHeaderCallback = (LayoutHeaderChange) mParentActivity;
                mFooterCallback = (LayoutFooterChange) mParentActivity;
            } catch (ClassCastException e) {
                throw new ClassCastException(mParentActivity.toString()
                        + " must implement LayoutHeaderChange and LayoutFooterChange");
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Set fragment attribute and inflate layout
        mView = inflater.inflate(R.layout.fragment_update, container, false);

        mResources = mParentActivity.getResources();

        mDatabaseUpdater = App.getDatabaseUpdateSingleton();

        // Change layout layout_portrait_header properties through MainActivity's interface LayoutHeaderChange
        mHeaderCallback.showButtonBack();
        mHeaderCallback.changeTitle(mResources.getString(R.string.fragment_update_header_title));
        mHeaderCallback.changeLogo(R.drawable.logo_database_white);
        mFooterCallback.closeFooter();

        /* ***********
        * Get views *
        ************/

        this.getViews();

        /* ***********
        * Set views *
        ************/

        this.setViews();

        // Set listeners
        this.setListeners();

        // Finally return the inflated view
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mDatabaseUpdater.addStateCallback(this);
        mDatabaseUpdater.addProgressCallback(this);
        mDatabaseUpdater.addVersionCallback(this);
        if(App.getDatabaseUpdateSingleton().isUpdateRunning()){
            runningUpdateScenario();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mDatabaseUpdater.removeStateCallback(this);
        mDatabaseUpdater.removeProgressCallback(this);
        mDatabaseUpdater.removeVersionCallback(this);
    }

    private void getViews() {
        fragment_update_textView_progress = mView.findViewById(R.id.fragment_update_textView_progress);
        fragment_update_textView_version = mView.findViewById(R.id.fragment_update_textView_version);
        fragment_update_textView_state = mView.findViewById(R.id.fragment_update_textView_state);
        fragment_update_imageView_update = mView.findViewById(R.id.fragment_update_imageView_update);
        fragment_update_imageView_check_update = mView.findViewById(R.id.fragment_update_imageView_check_update);
        fragment_update_imageView_delete = mView.findViewById(R.id.fragment_update_imageView_delete);
        fragment_update_progressBar_wait = mView.findViewById(R.id.fragment_update_progressBar_wait);
        fragment_update_progressBar_update = mView.findViewById(R.id.fragment_update_progressBar_update);
        fragment_update_constraintView_update = mView.findViewById(R.id.fragment_update_constraintView_update);
        fragment_update_recyclerView = mView.findViewById(R.id.fragment_update_recyclerView);
    }

    private void setViews() {
        // Set current version to textView
        setTextViewClientVersion();

        // Set update progress bar
        fragment_update_progressBar_update.setIndeterminate(true);

        // set recyclerView and adapter
        //fragment_update_recyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(mAppContext);
        fragment_update_recyclerView.setLayoutManager(mLayoutManager);

        mAllEntities = new ArrayList<>();
        mAdapter = new UpdateProgressEntityListAdapter(mAllEntities);
        fragment_update_recyclerView.setAdapter(mAdapter);

        //mView.findViewById(R.id.fragment_update_constraintView).requestLayout();

        /*mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);*/
    }

    private void setListeners() {

        // Update database
        fragment_update_imageView_update.setOnClickListener(v -> updateScenario());

        // Check server version
        fragment_update_imageView_check_update.setOnClickListener(v -> checkUpdateScenario());

        // Delete DB
        fragment_update_imageView_delete.setOnClickListener(v -> {
            mDatabaseUpdater.deleteAll();
            noClientVersionScenario();
            setTextViewClientVersion();
        });

        // Stop operations
        fragment_update_progressBar_wait.setOnClickListener(v -> {
            hideProgressBarWait();
            showButtonCheckUpdate();
            notifyUnknown();
        });
    }

    private void showButtonUpdate(){
        fragment_update_imageView_update.setVisibility(View.VISIBLE);
    }

    private void hideButtonUpdate(){
        fragment_update_imageView_update.setVisibility(View.INVISIBLE);
    }

    private void showButtonCheckUpdate(){
        fragment_update_imageView_check_update.setVisibility(View.VISIBLE);
    }

    private void hideButtonCheckUpdate(){
        fragment_update_imageView_check_update.setVisibility(View.INVISIBLE);
    }

    private void showUpdateProgressLayout(){
        fragment_update_constraintView_update.setVisibility(View.VISIBLE);
    }

    private void hideUpdateProgressLayout(){
        fragment_update_constraintView_update.setVisibility(View.GONE);
    }

    private void showProgressBarWait(){
        fragment_update_progressBar_wait.setVisibility(View.VISIBLE);
    }

    private void hideProgressBarWait(){
        fragment_update_progressBar_wait.setVisibility(View.INVISIBLE);
    }

    private void showTextViewState(){
        fragment_update_textView_state.setVisibility(View.VISIBLE);
    }

    private void clearProgressList(){
        mAdapter.clear();
    }

    private void setTextViewClientVersion(){
        setTextViewClientVersion(mDatabaseUpdater.getClientDatabaseVersion());
    }

    private void setTextViewClientVersion(DatabaseVersion version){
        String text;
        if(version!=null){
            text = version.toString();
        }
        else{
            text = mResources.getString(R.string.fragment_update_empty_local_db);
            noClientVersionScenario();
        }
        fragment_update_textView_version.setText(text);
    }

    private void setTextViewState(String text){
        fragment_update_textView_state.setText(text);
    }
    private void setTextViewState(String text, int color){
        setTextViewState(text);
        fragment_update_textView_state.setTextColor(color);
    }

    private void setTextViewProgress(int current, int max){
        fragment_update_textView_progress.setText(mAppContext.getString(R.string.fragment_update_progress, current, max));
    }

    private void runningUpdateScenario(){
        hideButtonUpdate();
        showUpdateProgressLayout();
        clearProgressList();
        showProgressBarWait();
    }

    private void updateScenario(){
        hideButtonUpdate();
        showUpdateProgressLayout();
        clearProgressList();
        showProgressBarWait();
        mDatabaseUpdater.update();
    }

    private void checkUpdateScenario(){
        hideButtonCheckUpdate();
        hideUpdateProgressLayout();
        showProgressBarWait();
        mDatabaseUpdater.checkVersion();
    }

    private void noClientVersionScenario(){
        hideButtonCheckUpdate();
        hideProgressBarWait();
        hideUpdateProgressLayout();
        showButtonUpdate();
        showTextViewState();
        setTextViewState(mResources.getString(R.string.fragment_update_state_empty), mResources.getColor(R.color.red));
        ((MainActivity) getActivity()).disableDrawerButtons();
    }

    private void notifyUpToDateScenario(){
        hideProgressBarWait();
        showButtonCheckUpdate();
        showTextViewState();
        setTextViewState(mResources.getString(R.string.fragment_update_state_uptodate), mResources.getColor(R.color.green));
    }

    private void notifyUnknownScenario(){
        hideUpdateProgressLayout();
        showButtonCheckUpdate();
        showTextViewState();
        setTextViewState(mResources.getString(R.string.fragment_update_state_unknown), mResources.getColor(R.color.red));
    }

    private void notifyUpdateScenario(){
        hideUpdateProgressLayout();
        showButtonUpdate();
        showTextViewState();
        setTextViewState(mResources.getString(R.string.fragment_update_state_update), mResources.getColor(R.color.orange));
    }

    @Override
    public void notifyUpToDate() {
        mParentActivity.runOnUiThread(this::notifyUpToDateScenario);
    }

    @Override
    public void notifyUnknown() {
        mParentActivity.runOnUiThread(this::notifyUnknownScenario);
    }

    @Override
    public void notifyUpdate() {
        mParentActivity.runOnUiThread(this::notifyUpdateScenario);
    }

    @Override
    public void notifyVersion(DatabaseVersion version) {
        mParentActivity.runOnUiThread(() -> setTextViewClientVersion(version));
    }

    @Override
    public void notifyProgress(int progress, int max) {
        mParentActivity.runOnUiThread(() -> {
            fragment_update_progressBar_update.setProgress(progress);
            fragment_update_progressBar_update.setMax(max);
            fragment_update_progressBar_update.setIndeterminate(false);
            setTextViewProgress(progress, max);
        });
    }

    @Override
    public void notifyProcessedEntity(Entity entity) {
        mAllEntities.add(entity);
        mParentActivity.runOnUiThread(() -> {
            mAdapter.notifyDataSetChanged();
            mLayoutManager.scrollToPosition(mAdapter.getItemCount());
        });
    }

    @Override
    public void notifyUpdateFinished() {
        this.notifyUpToDate();
    }
}
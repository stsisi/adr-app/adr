package fr.gendarmerie.stsisi.adr.interfaces.network;

import com.android.volley.VolleyError;

import org.json.JSONObject;

public interface VolleyResultNotifier {
    void notifyVersionReceived(JSONObject response);
    void notifyJsonObjectReceived(JSONObject response);
    void notifyNetworkError(String requestType, VolleyError error);
}

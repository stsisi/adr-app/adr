package fr.gendarmerie.stsisi.adr.db.entities;

import android.arch.persistence.room.*;
import android.support.annotation.*;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

@Entity(tableName = "kemler")
public class Kemler extends fr.gendarmerie.stsisi.adr.db.Entity {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    @Ignore
    private static final String TAG = Kemler.class.getSimpleName();

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private Integer mId=0;

    @NonNull
    @ColumnInfo(name = "code")
    private String mCode="";

    @NonNull
    @ColumnInfo(name = "name")
    private String mName="";

    public Kemler(@NonNull Integer id, @NonNull String code, @NonNull String name) {
        this.mId = id;
        this.mCode = code;
        this.mName = name;
        setEntityAttribute();
    }

    public Kemler(@NonNull JSONObject json){
        try {
            this.mId = json.getInt("id");
            this.mCode = json.getString("code");
            this.mName = json.getString("name");
            setEntityAttribute();
        }
        catch (JSONException exception){
            Log.e(TAG, "Error parsing JSON : "+exception.getCause()+" "+exception.getMessage());
        }
    }

    private void setEntityAttribute(){
        this.mIdentification = Integer.toString(mId);
        this.mTableName = this.getClass().getSimpleName();
        this.mShortDescription = this.mCode+" "+this.mName;
    }

    public Integer getId(){
        return this.mId;
    }

    public String getCode(){
        return this.mCode;
    }

    public String getName(){
        return this.mName;
    }
}
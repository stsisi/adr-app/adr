package fr.gendarmerie.stsisi.adr.interfaces.db;

import fr.gendarmerie.stsisi.adr.db.Container;

/**
 * Interface use to notify the result of database request
 *
 * @author remy.persuanne
 * @version 1.0
 */
public interface DatabaseRequestResultNotifier {

    /**
     * Caller notifies implementors that the request return result
     *
     * @param container the result of the request
     */
    void notifyRequestResult(Container container);

    /**
     * Caller notifies implementors that the request return nothing
     */
    void notifyRequestEmpty();
}

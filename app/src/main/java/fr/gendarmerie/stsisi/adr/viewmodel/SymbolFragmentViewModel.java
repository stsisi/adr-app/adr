package fr.gendarmerie.stsisi.adr.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

import fr.gendarmerie.stsisi.adr.db.Container;
import fr.gendarmerie.stsisi.adr.db.entities.Symbol;
import fr.gendarmerie.stsisi.adr.interfaces.db.DatabaseRequestResultNotifier;
import fr.gendarmerie.stsisi.adr.repository.LocalRepository;

public class SymbolFragmentViewModel extends AndroidViewModel {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    private static final String TAG = SymbolFragmentViewModel.class.getSimpleName();

    private final LocalRepository mRepository;

    private final Container mResult = new Container();

    private final DatabaseRequestResultNotifier mCallBack;

    public SymbolFragmentViewModel(Application application, DatabaseRequestResultNotifier callBack){
        super(application);
        mRepository = new LocalRepository(application);
        mCallBack = callBack;
    }

    public void getSymbolByName(String uri){
        AsyncTask.execute(() -> {
            if(mCallBack != null) {
                List<Symbol> symbols = mRepository.getSymbolByUri(uri);

                if(!symbols.isEmpty()){
                    mResult.setSymbols(symbols);
                    mCallBack.notifyRequestResult(mResult);
                }
                else{
                    mCallBack.notifyRequestEmpty();
                }
            }
            else{
                Log.w(TAG, "getSymbolByName : No call back interface set, so no call back made... ^_^");
            }
        });
    }


}

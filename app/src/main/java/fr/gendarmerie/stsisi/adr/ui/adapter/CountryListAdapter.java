package fr.gendarmerie.stsisi.adr.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import fr.gendarmerie.stsisi.adr.R;
import fr.gendarmerie.stsisi.adr.db.entities.Country;

public class CountryListAdapter extends RecyclerView.Adapter<CountryListAdapter.CountryViewHolder> {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    private static final String TAG = CountryListAdapter.class.getSimpleName();

    class CountryViewHolder extends RecyclerView.ViewHolder {
        private final TextView adapter_list_textView_country;
        private final ImageView adapter_list_imageView_flag;

        private CountryViewHolder(View itemView) {
            super(itemView);
            adapter_list_textView_country = itemView.findViewById(R.id.adapter_list_textView_country);
            adapter_list_imageView_flag = itemView.findViewById(R.id.adapter_list_imageView_flag);
        }

        private void showFlag(){
            adapter_list_imageView_flag.setVisibility(View.VISIBLE);
        }

        private void hideFlag(){
            adapter_list_imageView_flag.setVisibility(View.INVISIBLE);
        }

        private void setCountryFlag(Drawable flag){
            adapter_list_imageView_flag.setImageDrawable(flag);
            showFlag();
        }

        private void setCountryName(String name){
            adapter_list_textView_country.setText(name);
        }
    }

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final Resources mResources;
    private List<Country> mCountries; // Cached copy of countries

    public CountryListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        mContext=context;
        mResources = context.getResources();
    }

    @Override
    public @NonNull CountryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.adapter_list_country, parent, false);
        return new CountryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CountryViewHolder holder, int position) {
        if (mCountries != null) {
            Country current = mCountries.get(position);
            holder.setCountryName(current.getName());

            String uri = current.getFlagUri();
            int id = mResources.getIdentifier(uri, "drawable", mContext.getPackageName());
            if (id != 0) {
                holder.setCountryFlag(mResources.getDrawable(id));
            }
            else {
                holder.hideFlag();
                Log.w(TAG, "No drawable found with name " + uri);
            }
        }
        else {
            // Covers the case of data not being ready yet.
            holder.setCountryName(mResources.getString(R.string.fragment_country_no_country));
        }
    }

    public void setCountries(List<Country> countries){
        mCountries = countries;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mCountries has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mCountries != null)
            return mCountries.size();
        else return 0;
    }
}

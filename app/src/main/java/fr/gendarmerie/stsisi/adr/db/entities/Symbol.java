package fr.gendarmerie.stsisi.adr.db.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

@Entity(tableName = "symbol")
public class Symbol extends fr.gendarmerie.stsisi.adr.db.Entity {

    /**
     * @value #STATIC_FIELD tag consisting of the name of the class, used to identify it.
     */
    @Ignore
    private static final String TAG = Symbol.class.getSimpleName();

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private Integer mId=0;

    @ColumnInfo(name = "code")
    private String mCode;

    @NonNull
    @ColumnInfo(name = "name")
    private String mName="";

    @NonNull
    @ColumnInfo(name = "uri")
    private String mUri ="";

    @ColumnInfo(name = "hazard")
    private String mHazard;

    @ColumnInfo(name = "guidance")
    private String mGuidance;

    @ColumnInfo(name = "description")
    private String mDescription;


    public Symbol(@NonNull Integer id, String code, @NonNull String name, @NonNull String uri, String hazard, String guidance, String description) {
        this.mId = id;
        this.mCode = code;
        this.mName = name;
        this.mUri = uri;
        this.mHazard = hazard;
        this.mGuidance = guidance;
        this.mDescription = description;
        setEntityAttribute();
    }

    public Symbol(@NonNull JSONObject json){
        try {
            this.mId = json.getInt("id");
            this.mCode = json.getString("code");
            this.mName = json.getString("name");
            this.mUri = json.getString("uri");
            this.mHazard = json.getString("hazard");
            this.mGuidance = json.getString("guidance");
            this.mDescription = json.getString("description");
            setEntityAttribute();
        }
        catch (JSONException exception){
            Log.e(TAG, "Error parsing JSON : "+exception.getCause()+" "+exception.getMessage());
        }
    }

    private void setEntityAttribute(){
        this.mIdentification = Integer.toString(mId);
        this.mTableName = this.getClass().getSimpleName();
        this.mShortDescription = this.mName;
    }

    @NonNull
    public Integer getId() {
        return this.mId;
    }

    @NonNull
    public String getName() {
        return mName;
    }

    @NonNull
    public String getUri() {
        return mUri;
    }

    public String getHazard() {
        return mHazard;
    }

    public String getGuidance() {
        return mGuidance;
    }

    public String getCode() {
        return mCode;
    }

    public String getDescription() {
        return mDescription;
    }

}
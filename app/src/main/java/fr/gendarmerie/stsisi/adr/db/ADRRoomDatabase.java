package fr.gendarmerie.stsisi.adr.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import fr.gendarmerie.stsisi.adr.db.dao.Dao;
import fr.gendarmerie.stsisi.adr.db.entities.Category;
import fr.gendarmerie.stsisi.adr.db.entities.Classification;
import fr.gendarmerie.stsisi.adr.db.entities.Country;
import fr.gendarmerie.stsisi.adr.db.entities.Group;
import fr.gendarmerie.stsisi.adr.db.entities.HasSymbol;
import fr.gendarmerie.stsisi.adr.db.entities.Hazard;
import fr.gendarmerie.stsisi.adr.db.entities.Kemler;
import fr.gendarmerie.stsisi.adr.db.entities.Material;
import fr.gendarmerie.stsisi.adr.db.entities.Name;
import fr.gendarmerie.stsisi.adr.db.entities.State;
import fr.gendarmerie.stsisi.adr.db.entities.Symbol;
import fr.gendarmerie.stsisi.adr.db.entities.Tunnel;
import fr.gendarmerie.stsisi.adr.db.entities.Type;

import static fr.gendarmerie.stsisi.adr.utilities.ConstantsKt.DATABASE_NAME;

@Database(
    entities = {
        Category.class,
        Classification.class,
        Country.class,
        Group.class,
        HasSymbol.class,
        Hazard.class,
        Kemler.class,
        Material.class,
        Name.class,
        State.class,
        Symbol.class,
        Tunnel.class,
        Type.class
    },
    version = 1,
    exportSchema = false)
public abstract class ADRRoomDatabase extends RoomDatabase {

    public abstract Dao dao();

    private static volatile ADRRoomDatabase instance;

    public static ADRRoomDatabase getDatabase(final Context context) {
        if (instance == null) {
            synchronized (ADRRoomDatabase.class) {
                if (instance == null) {
                    instance = Room.databaseBuilder(context.getApplicationContext(),
                            ADRRoomDatabase.class, DATABASE_NAME)
                            .build();
                }
            }
        }
        return instance;
    }
}

package fr.gendarmerie.stsisi.adr.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import fr.gendarmerie.stsisi.adr.R;
import fr.gendarmerie.stsisi.adr.db.Entity;

public class UpdateProgressEntityListAdapter extends RecyclerView.Adapter<UpdateProgressEntityListAdapter.EntityUpdateViewHolder> {

    class EntityUpdateViewHolder extends RecyclerView.ViewHolder {
        private final TextView adapter_list_entity_update_textView_table_name;
        private final TextView adapter_list_entity_update_textView_id;
        private final TextView adapter_list_entity_update_textView_description;

        private EntityUpdateViewHolder(View itemView) {
            super(itemView);
            adapter_list_entity_update_textView_table_name = itemView.findViewById(R.id.adapter_list_entity_update_textView_table_name);
            adapter_list_entity_update_textView_id = itemView.findViewById(R.id.adapter_list_entity_update_textView_id);
            adapter_list_entity_update_textView_description = itemView.findViewById(R.id.adapter_list_entity_update_textView_description);
        }
    }

    private final List<Entity> mEntities;

    public UpdateProgressEntityListAdapter(@NonNull List<Entity> entities) {
        mEntities = entities;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public @NonNull UpdateProgressEntityListAdapter.EntityUpdateViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_list_entity_progress_update, parent, false);
        return new UpdateProgressEntityListAdapter.EntityUpdateViewHolder(itemView);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull UpdateProgressEntityListAdapter.EntityUpdateViewHolder holder, int position) {
        Entity current = mEntities.get(position);
        holder.adapter_list_entity_update_textView_table_name.setText(current.getTableName());
        holder.adapter_list_entity_update_textView_id.setText(current.getIdentification());
        holder.adapter_list_entity_update_textView_description.setText(current.getShortDescription());
    }

    // getItemCount() is called many times, and when it is first called,
    // mEntities has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        return mEntities.size();
    }

    public void clear() {
        final int size = mEntities.size();
        mEntities.clear();
        notifyItemRangeRemoved(0, size);
    }
}

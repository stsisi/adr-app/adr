package fr.gendarmerie.stsisi.adr.interfaces.search;

import java.util.List;

import fr.gendarmerie.stsisi.adr.utilities.Item;

public interface SearchResultNotifier {
    void notifySearchResult(List<Item> results);
    void notifySearchEmpty();
}

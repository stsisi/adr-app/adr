package fr.gendarmerie.stsisi.adr.utilities;

import java.io.Serializable;
import java.util.List;

import fr.gendarmerie.stsisi.adr.db.entities.Category;
import fr.gendarmerie.stsisi.adr.db.entities.Classification;
import fr.gendarmerie.stsisi.adr.db.entities.Group;
import fr.gendarmerie.stsisi.adr.db.entities.Hazard;
import fr.gendarmerie.stsisi.adr.db.entities.Kemler;
import fr.gendarmerie.stsisi.adr.db.entities.Material;
import fr.gendarmerie.stsisi.adr.db.entities.Name;
import fr.gendarmerie.stsisi.adr.db.entities.State;
import fr.gendarmerie.stsisi.adr.db.entities.Symbol;
import fr.gendarmerie.stsisi.adr.db.entities.Tunnel;
import fr.gendarmerie.stsisi.adr.db.entities.Type;
import fr.gendarmerie.stsisi.adr.repository.LocalRepository;

public class Item implements Serializable {

    private Category mCategory;
    private Classification mClassification;
    private Group mGroup;
    private Hazard mHazard;
    private Kemler mKemler;
    private final Material mMaterial;
    private Name mName;
    private List<Symbol> mSymbols;
    private State mState;
    private Tunnel mTunnel;
    private Type mType;

    public Item(){
        this.mMaterial = null;
    }

    public Item(Material material){
        this.mMaterial = material;
        this.hydrate();
    }

    private void hydrate(){
        this.mCategory = App.getLocalRepository().getCategoryByMaterial(this.mMaterial.getId());
        this.mClassification = App.getLocalRepository().getClassificationByMaterial(this.mMaterial.getId());
        if(mClassification!=null){
            this.mHazard = App.getLocalRepository().getHazardByClassification(this.mClassification.getId());
            this.mState = App.getLocalRepository().getStateByClassification(this.mClassification.getId());
            this.mType = App.getLocalRepository().getTypeByClassification(this.mClassification.getId());
        }
        this.mGroup = App.getLocalRepository().getGroupByMaterial(this.mMaterial.getId());
        this.mKemler = App.getLocalRepository().getKemlerByMaterial(this.mMaterial.getId());
        this.mName = App.getLocalRepository().getNameByMaterial(this.mMaterial.getId());
        this.mSymbols = App.getLocalRepository().getAllSymbolByMaterial(this.mMaterial.getId());
        this.mTunnel = App.getLocalRepository().getTunnelByMaterial(this.mMaterial.getId());
    }

    public Material getMaterial() {
        return mMaterial;
    }

    public Category getCategory() {
        return mCategory;
    }

    public Classification getClassification() {
        return mClassification;
    }

    public Group getGroup() {
        return mGroup;
    }

    public Hazard getHazard() {
        return mHazard;
    }

    public Kemler getKemler() {
        return mKemler;
    }

    public Name getName() {
        return mName;
    }

    public State getState() {
        return mState;
    }

    public List<Symbol> getSymbols() {
        return mSymbols;
    }

    public Tunnel getTunnel() {
        return mTunnel;
    }

    public Type getType() {
        return mType;
    }

}
